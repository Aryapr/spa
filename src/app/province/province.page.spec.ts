import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProvincePage } from './province.page';

describe('ProvincePage', () => {
  let component: ProvincePage;
  let fixture: ComponentFixture<ProvincePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProvincePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProvincePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
