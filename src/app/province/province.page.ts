import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-province',
  templateUrl: './province.page.html',
  styleUrls: ['./province.page.scss'],
})
export class ProvincePage implements OnInit {

  @Input() province: string;
  @Input() img: string;

  constructor(
    private modalController: ModalController
  ) { }

  ngOnInit() {
  }
  dismissModal() {
    this.modalController.dismiss();
  }

}
