import { Component, OnInit} from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { ContentService } from '../services/content.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateConfigService } from '../services/translate-config.service';
import { Storage } from '@ionic/storage';
import { Platform } from '@ionic/angular';
import { File } from '@ionic-native/File/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { FileTransfer, FileTransferObject  } from '@ionic-native/file-transfer/ngx';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer/ngx';

@Component({
  selector: 'app-map-data',
  templateUrl: './map-data.page.html',
  styleUrls: ['./map-data.page.scss'],
})
export class MapDataPage implements OnInit {
  data: any;
  ids: any;
  compareStatus: boolean;
  state: any;
  subscribeObj;
  subscribeObj1;
  subscribeObj2;
  selectedFontSize  = 1;
  html: string;
  appLang = '';
  constructor(
    private content: ContentService,
    public loadingController: LoadingController,
    private route: ActivatedRoute,
    private router: Router,
    private translateConfigService: TranslateConfigService,
    private storage: Storage,
    private document: DocumentViewer,
    private platform: Platform,
    private file: File,
    private ft: FileTransfer, 
    private fileOpener: FileOpener,
  ) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.ids = this.router.getCurrentNavigation().extras.state.ids;
        this.compareStatus = this.router.getCurrentNavigation().extras.state.compareStatus;
        this.state =  this.router.getCurrentNavigation().extras.state.state;
      }
    });

  }
  // ionViewDidEnter() {
  //   this.subscribeObj = this.translateConfigService.selectedLanguage.subscribe(
  //     lang => {
  //       this.appLang = lang;
  //       this.getContents();
  //     }
  //   );
  ionViewWillEnter() {
    // Load the data
    this.subscribeObj = this.translateConfigService.selectedLanguage.subscribe(lang => {
      this.getContents();
    });
    this.subscribeObj1 = this.translateConfigService.selectedFontSize.subscribe(size => {
      this.selectedFontSize = size;
      document.body.style.setProperty('--pixels', this.selectedFontSize.toString());
    });
    this.subscribeObj2 = this.translateConfigService.selectedLanguage.subscribe(
      lang => {
        this.appLang = lang;
        this.getContents();
      }
    );
  }
  ionViewWillLeave() {
    this.subscribeObj.unsubscribe();
    this.subscribeObj1.unsubscribe();
    this.subscribeObj2.unsubscribe();
  }

  ngOnInit() {
  }
  async getContents() {

    const loading = await this.loadingController.create({
      message: this.translateConfigService.getMessages('loading')
    });

    /** Get data from local storage */
    const lang = this.translateConfigService.selected;
    const storageSlug   =   'map' + '_' + '_' + lang + this.ids + '_' + this.state + '_' + this.compareStatus;
  //  this.data = await this.storage.get(storageSlug);

    if (!this.data) {
      await loading.present();
    }
    await this.content.getMapContents(this.ids, this.state, this.compareStatus)
      .then(
        data => {
          // Set the data to display in the template
          if (data) {
            this.data = data;
          } else {
            this.data = '<div class="map-img-alert">' + this.translateConfigService.getMessages( 'noData' ) + '<div>';
          }
          loading.dismiss();
        },
        err => {
          console.log(err);
          loading.dismiss();
        }
    );
  }
  async getImage(image_url){
    const loading = await this.loadingController.create({
      message: this.translateConfigService.getMessages('loading') 
    });
    await loading.present();
    const path = this.file.dataDirectory;
    const fileTransfer: FileTransferObject  = this.ft.create();

    let fileExtn=image_url.split('.').reverse()[0];
    let fileMIMEType=this.getMIMEtype(fileExtn);


    fileTransfer.download(image_url, path + 'download.'+fileExtn).then((entry) => {
      //  console.log('download complete: ' + entry.toURL());
      const url = entry.toURL();
      loading.dismiss();
      if (this.platform.is('ios')) {
        this.document.viewDocument(url, fileMIMEType, {});
      } else {
        this.fileOpener.open(url, fileMIMEType)
          .then(() => console.log('File is opened'))
          .catch(e => console.log('Error opening file', e));
      }

    }, (error) => {
      console.log(error);
    });
  }

  getMIMEtype(extn){
    let ext=extn.toLowerCase();
    let MIMETypes={
      'txt' :'text/plain',
      'docx':'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      'doc' : 'application/msword',
      'pdf' : 'application/pdf',
      'jpg' : 'image/jpeg',
      'bmp' : 'image/bmp',
      'png' : 'image/png',
      'xls' : 'application/vnd.ms-excel',
      'xlsx': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      'rtf' : 'application/rtf',
      'ppt' : 'application/vnd.ms-powerpoint',
      'pptx': 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
    }
    return MIMETypes[ext];
  }
}