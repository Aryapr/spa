import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MapDataPage } from './map-data.page';

const routes: Routes = [
  {
    path: '',
    component: MapDataPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MapDataPageRoutingModule {}
