import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MapDataPage } from './map-data.page';

describe('MapDataPage', () => {
  let component: MapDataPage;
  let fixture: ComponentFixture<MapDataPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapDataPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MapDataPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
