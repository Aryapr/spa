import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MapDataPageRoutingModule } from './map-data-routing.module';

import { MapDataPage } from './map-data.page';

import { PipesModule } from '../pipes/pipe.module';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MapDataPageRoutingModule,
    PipesModule,
    TranslateModule
  ],
  declarations: [MapDataPage]
})
export class MapDataPageModule {}
