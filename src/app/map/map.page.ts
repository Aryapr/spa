import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';

import { ContentService } from '../services/content.service';
import { TranslateConfigService } from '../services/translate-config.service';
import { Storage } from '@ionic/storage'; 
@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit {
  data: any = '';
  subscribeObj;
  subscribeObj1;
  selectedFontSize  = 1;
  noData = '';
  constructor(
    private content: ContentService,
    public loadingController: LoadingController,
    private route: ActivatedRoute,
    private router: Router,
    private translateConfigService: TranslateConfigService,
    private storage: Storage,
  ) { }
  ionViewWillEnter() {
    // Load the data
    this.subscribeObj  = this.translateConfigService.selectedLanguage.subscribe(lang => {
      this.getContentsMap();
    });
    this.subscribeObj1 = this.translateConfigService.selectedFontSize.subscribe(size => {
      this.selectedFontSize = size;
      document.body.style.setProperty('--pixels', this.selectedFontSize.toString());
    });
  }
  ionViewWillLeave() {
    this.subscribeObj1.unsubscribe();
    this.subscribeObj.unsubscribe();
  }
  ngOnInit() {

  }

  async getContentsMap() {
    this.noData = '';
    const loading = await this.loadingController.create({
      message: this.translateConfigService.getMessages('loading')
    });

    /** Get data from local storage */
    const page = 'map';
    const lang = this.translateConfigService.selected;
    const storageSlug   =   page + '_' + lang;
  //  this.data = await this.storage.get(storageSlug);

    if (!this.data) {
      await loading.present();
    }
    await this.content.getContents(page)
      .then(
        data => {

          // Set the data to display in the template
          if (data) {
            this.data = data;
          }
          loading.dismiss();
          this.noData = this.translateConfigService.getMessages('noData');
        },
        err => {
          console.log(err);
          loading.dismiss();
          this.noData = this.translateConfigService.getMessages('noData');
        }
    );
  }
}
