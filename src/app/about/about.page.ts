import { Component, OnInit } from '@angular/core';
import { ContentService } from '../services/content.service';
import { LoadingController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateConfigService } from '../services/translate-config.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {

  aboutData = {
    post_title: '',
    post_content: '',
    banner_image: '../../assets/images/about.png',
    organization_content: '',
    button_name: 'Register'
  };
  slideOpts = {
    initialSlide: 1,
    speed: 400
  };
  selectedLanguage: string;
  loading;
  subscribeObj;
  subscribeObj1;
  selectedFontSize  = 1;
  nativeData1;
  nativeData;
  noData = '';
  constructor(
    private content: ContentService,
    public loadingController: LoadingController,
    private translateConfigService: TranslateConfigService,
    private route: ActivatedRoute,
    private router: Router,
    private storage: Storage
  ) {

  }

  ionViewDidEnter() {

    this.subscribeObj = this.translateConfigService.selectedLanguage.subscribe(lang => {
      // Load the data
      this.getContents();
    });
    this.subscribeObj1 = this.translateConfigService.selectedFontSize.subscribe(size => {
      this.selectedFontSize = size;
      document.body.style.setProperty('--pixels', this.selectedFontSize.toString());
    });

  }

  ionViewWillLeave() {
    this.subscribeObj.unsubscribe();
    this.subscribeObj1.unsubscribe();
    const vid: any = document.getElementById('videoId1');
    vid.pause();
  }

  async getContents() {

    this.noData = '';
    const loading = await this.loadingController.create({
      message: this.translateConfigService.getMessages('loading')
    });

    /** Get data from local storage */
    const page = 'about-us';
    const lang = this.translateConfigService.selected;
    const storageSlug   =   'about' + '_' + lang;

    // const aboutData = await this.storage.get(storageSlug);
    // if (this.aboutData) {
    //   this.aboutData = this.aboutData[0];
    // } else {
    //   this.aboutData = {
    //     post_title: '',
    //     post_content: '',
    //     banner_image: '../../assets/images/about.png',
    //     organization_content: '',
    //     button_name: 'Register'
    //   };
    // }

    // if (!this.aboutData.post_title) {
    //   await loading.present();
    // }

    await loading.present();
    await this.content.getContents(page)
      .then(
        data => {
          console.log(data);
          // Set the data to display in the template
          if (data) {
            this.aboutData = data[0];
          }
          loading.dismiss();
          this.noData = this.translateConfigService.getMessages('noData');
        },
        err => {
          console.log(err);
          loading.dismiss();
          this.noData = this.translateConfigService.getMessages('noData');
        }
    );
    // }
  }

  ngOnInit() {
  }

}
