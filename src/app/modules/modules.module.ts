import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModulesPageRoutingModule } from './modules-routing.module';

import { ModulesPage } from './modules.page';

import { TranslateModule } from '@ngx-translate/core';
import { PipesModule } from '../pipes/pipe.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModulesPageRoutingModule,
    TranslateModule,
    PipesModule
  ],
  declarations: [ModulesPage]
})
export class ModulesPageModule {}
