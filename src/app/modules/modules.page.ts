import { Component, OnInit } from '@angular/core';
import { ModulesService } from '../services/modules.service';
import { AuthService } from 'src/app/services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { TranslateConfigService } from '../services/translate-config.service';
import { Storage } from '@ionic/storage';

import { Platform } from '@ionic/angular';
import { File } from '@ionic-native/File/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer/ngx';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-modules',
  templateUrl: './modules.page.html',
  styleUrls: ['./modules.page.scss'],
})
export class ModulesPage implements OnInit {

  modules: any = '';
  lessonUrl = 'lessons/';
  isLoggedIn: boolean;
  subscribeObj;
  subscribeObj1;
  selectedFontSize  = 1;
  noData = '';
  lessonId;
  moduleId;
  quizInfo;
  lastLesson: any;
  nextLesson: any;
  quizForm: FormGroup;
  quizResponse;
  right = 0;
  total = 0;
  msg: any;
  msgCls = '';
  quizResponseStatus = false;
  alphabets = [];
  certResponse: any;
  messages;
  // subscribeObj;
  // noData = '';
  // subscribeObj1;
  // selectedFontSize  = 1;
  constructor(
    private module: ModulesService,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private navCtrl: NavController,
    public loadingController: LoadingController,
    private translateConfigService: TranslateConfigService,
    private storage: Storage,
    private formBuilder: FormBuilder,
    private platform: Platform,
    private file: File,
    private ft: FileTransfer,
    private fileOpener: FileOpener,
    private document: DocumentViewer,
  ) {
    // this.moduleId = this.route.snapshot.paramMap.get('mid');
    // this.lessonId = this.route.snapshot.paramMap.get('lid');
  }

  ionViewWillEnter() {
    this.authService.isLoggedIn.subscribe(state => {
      this.isLoggedIn = state;
    });
    // Load the data
    this.subscribeObj = this.translateConfigService.selectedLanguage.subscribe(lang => {
      this.getModules();
    });
    this.subscribeObj1 = this.translateConfigService.selectedFontSize.subscribe(size => {
      this.selectedFontSize = size;
      document.body.style.setProperty('--pixels', this.selectedFontSize.toString());
    });
  }
  ionViewWillLeave() {
    this.subscribeObj.unsubscribe();
    this.subscribeObj1.unsubscribe();
  }

  async getModules() {
    this.noData = '';
    const loading = await this.loadingController.create({
      message: this.translateConfigService.getMessages('loading')
    });

    /** Get data from local storage */
    const lang  = this.translateConfigService.selected;
    const storageSlug   =   'module' + '_' + lang;
  //  this.modules = await this.storage.get(storageSlug);

    if (!this.modules) {
      await loading.present();
    }
    await this.module.getModules()
      .then(
        data => {
          // Set the data to display in the template
          if (data) {
            this.modules = data;
          }
          console.log(data);
          loading.dismiss();
          this.noData = this.translateConfigService.getMessages('noData');
        },
        err => {
          console.log(err);
          loading.dismiss();
          this.noData = this.translateConfigService.getMessages('noData');
        }
    );
  }
  ngOnInit() {
  }


}
