import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';

import { PopoverController } from '@ionic/angular';
import { RegisterPopoverComponent } from '../widgets/register-popover/register-popover.component';

import { AuthService } from 'src/app/services/auth.service';
import { AlertService } from 'src/app/services/alert.service';

import { ConfigurationOptions, OutputOptionsEnum } from 'intl-input-phone';

import { TranslateConfigService } from '../services/translate-config.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  registerForm: FormGroup;
  userData: any;
  myDate = new Date();
  radiocheck: any;
  emailDisabled = true;
  emailChecked = true;
  textDisabled = true;
  textChecked = false;

  configOption1: ConfigurationOptions;
  datePickerObj: any = {
    inputDate: new Date(), // default new Date()
    fromDate: new Date('1890-01-01'), // default null
    toDate: new Date(), // default null
    showTodayButton: false, // default true
    closeOnSelect: true, // default false
    mondayFirst: true, // default false
    setLabel: 'Set',  // default 'Set'
    todayLabel: 'Today', // default 'Today'
    closeLabel: 'Close', // default 'Close'
    titleLabel: 'Select a Date', // default null
    monthsList: ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
    weeksList: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
    dateFormat: 'YYYY-MM-DD', // default DD MMM YYYY
    clearButton : false , // default true
    momentLocale: 'pt-BR', // Default 'en-US'
    yearInAscending: true, // Default false
    btnCloseSetInReverse: true, // Default false
    btnProperties: {
      expand: 'block', // Default 'block'
      fill: '', // Default 'solid'
      size: '', // Default 'default'
      disabled: '', // Default false
      strong: '', // Default false
      color: '' // Default ''
    },
    arrowNextPrev: {
      nextArrowSrc: 'assets/images/arrow_right.svg',
      prevArrowSrc: 'assets/images/arrow_left.svg'
    }, // This object supports only SVG files.
    isSundayHighlighted : {
    fontColor: '#ee88bf' // Default null
    } // Default {}
  };
  selectedRadioGroup: any;
  subscribeObj1;
  selectedFontSize  = 1;
  messages;
constructor(
    private authService: AuthService,
    private navCtrl: NavController,
    private alertService: AlertService,
    private formBuilder: FormBuilder,
    public popoverController: PopoverController,
    private translateConfigService: TranslateConfigService
  ) {

    this.registerForm = this.formBuilder.group({
      first_name: ['', Validators.compose([Validators.required])],
      last_name : ['', Validators.compose([Validators.required])],
      uci_number : ['', Validators.compose([Validators.required])],
      dob : ['', Validators.compose([Validators.required])],
      email : ['', Validators.compose([Validators.email])],
      phone : ['', Validators.compose([Validators.maxLength(10)])],
      message_type : ['email', Validators.compose([Validators.required])],
      rec_check : ['false', Validators.compose([Validators.required])],
      oop_check : ['false', Validators.compose([Validators.required])],
      sopa_register : ['false', Validators.compose([Validators.required])]
    },
    {
      validators: this.emailPhoneValidation.bind(this)
    });

    this.configOption1 = new ConfigurationOptions();
    this.configOption1.OutputFormat = OutputOptionsEnum.Number;

  }

  emailPhoneValidation(formGroup: FormGroup) {

    const { value: email } = formGroup.get('email');
    const { value: phone } = formGroup.get('phone');
    if (email && phone) {
      this.emailChecked = true;
      this.textChecked = false;
      this.emailDisabled = false;
      this.textDisabled = false;
      return null;
    } else if (email) {
      this.emailChecked = true;
      this.textChecked = false;
      this.emailDisabled = false;
      this.textDisabled = true;
      return null;
    } else if (phone) {
      this.emailChecked = false;
      this.textChecked = true;
      this.emailDisabled = true;
      this.textDisabled = false;
      return null;
    } else {
      this.emailDisabled = true;
      this.textDisabled = true;
      this.emailChecked = true;
      this.textChecked = false;
      return { phoneEmailRequired: true };
    }
  }

  ionViewWillEnter() {
    this.authService.isLoggedIn.subscribe(state => {
      if (state) {
        this.navCtrl.navigateRoot('start/tabs/home');
      }
    });
    this.subscribeObj1 = this.translateConfigService.selectedFontSize.subscribe(size => {
      this.selectedFontSize = size;
      document.body.style.setProperty('--pixels', this.selectedFontSize.toString());
    });
  }
  ionViewWillLeave() {
    this.subscribeObj1.unsubscribe();
  }
  ngOnInit() {

  }

  register(form) {
    this.messages = this.translateConfigService.getMessages('REGISTER.messages');

    this.authService.register(form.value.first_name, form.value.last_name,
      form.value.uci_number, form.value.dob, form.value.email,
      form.value.phone, this.radiocheck, form.value.rec_check,
      form.value.oop_check, form.value.sopa_register).then(
      data => {
        this.userData = data;
        if (this.userData.status === 1) {
          this.navCtrl.navigateRoot('login');
        }

        this.alertService.presentToast(this.messages[this.userData.status]);
      },
      error => {
        console.log(error);
      }
    );
  }
  async presentPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: RegisterPopoverComponent,
      event: ev,
      translucent: true
    });
    return await popover.present();
  }
  checkValueradio(event) {
    this.radiocheck = event.detail.value;
  }

}
