import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Ionic4DatepickerModule } from '@logisticinfotech/ionic4-datepicker';

import { RegisterPageRoutingModule } from './register-routing.module';

import { RegisterPage } from './register.page';

import { TooltipsModule } from 'ionic-tooltips';
import { RegisterPopoverComponent } from '../widgets/register-popover/register-popover.component';

import { IntlInputPhoneModule } from 'intl-input-phone';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegisterPageRoutingModule,
    ReactiveFormsModule,
    Ionic4DatepickerModule,
    TooltipsModule.forRoot(),
    TranslateModule,
    IntlInputPhoneModule
    ,
  ],
  declarations: [RegisterPage, RegisterPopoverComponent],
  entryComponents: [ RegisterPopoverComponent ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RegisterPageModule {}
