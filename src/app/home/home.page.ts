import { Component, OnInit } from '@angular/core';
import { ContentService } from '../services/content.service';
import { LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { TranslateConfigService } from '../services/translate-config.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  homeData: any;
  slideOpts = {
    initialSlide: 1,
    speed: 400
  };

  selectedLanguage: string;
  loading;
  subscribeObj;
  subscribeObj1;
  nativeData1;
  nativeData;
  public checklists = [];
  noData = '';
  selectedFontSize  = 1;
  
  constructor(
    private content: ContentService,
    public loadingController: LoadingController,
    private translateConfigService: TranslateConfigService,
    private navCtrl: NavController,
    private storage: Storage,

  ) {
    this.noData = this.translateConfigService.getMessages('noData');
  }
  async submitQuiz(form) {
  }

  ionViewDidEnter() {

    this.subscribeObj = this.translateConfigService.selectedLanguage.subscribe(lang => {
      // Load the data
      this.getContents();
    });
    this.subscribeObj1 = this.translateConfigService.selectedFontSize.subscribe(size => {
      this.selectedFontSize = size;
      document.body.style.setProperty('--pixels', this.selectedFontSize.toString());
    });
  }

  ionViewWillLeave() {
    this.subscribeObj.unsubscribe();
    this.subscribeObj1.unsubscribe();
    const vid: any = document.getElementById('videoId1');
    vid.pause();
  }

  async getContents() {

    this.noData = '';
    const loading = await this.loadingController.create({
      message: this.translateConfigService.getMessages('loading')
    });

    /** Get data from local storage */
    const page = 'home';
    const lang = this.translateConfigService.selected;
    const storageSlug   =   page + '_' + lang;
    // this.homeData = await this.storage.get(storageSlug);

    if (!this.homeData) {
      await loading.present();
    }
    await this.content.getContents(page)
      .then(
        data => {
          /** Current language */
          const lg = this.translateConfigService.selected;
          if (data) {
            const homeData = data;
            /** if lang from server and current selected lang match update data to var */
            // if ( homeData[0].lang === lg ) {
            this.homeData = homeData;
            // }
            if (this.homeData) {
              this.homeData[1].list_data.forEach(item => {
                this.checklists.push( {name: item.list_item, checked: false});
              });
            } else {
              this.homeData = '';
            }
          }

          loading.dismiss();
          this.noData = this.translateConfigService.getMessages('noData');
        },
        err => {
          console.log('errrrr', err);
          loading.dismiss();
          this.noData = this.translateConfigService.getMessages('noData');
        }
    );
  }

  goToRegisterPage(event, selectedIndex) {

    const checked = event.target.checked;
    this.checklists.forEach( (item, index)  => {
      console.log(checked, selectedIndex === index );
      if ( selectedIndex === index ) {
        this.checklists[index].checked =  checked;
      }
    });
    if (this.checklists.filter(c => c.checked === false).length === 0 ) {
      this.navCtrl.navigateRoot('/register');
    }
    console.log(this.checklists);
  }
  ngOnInit() {
  }

}
