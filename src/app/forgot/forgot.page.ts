import { Component, OnInit } from '@angular/core';
// import { ModalController } from '@ionic/angular';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

import { NavController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { AlertService } from 'src/app/services/alert.service';

import { TranslateConfigService } from '../services/translate-config.service';
@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.page.html',
  styleUrls: ['./forgot.page.scss'],
})
export class ForgotPage implements OnInit {

  forgotForm: FormGroup;
  userData: any;
  messages: any;
  subscribeObj1;
  selectedFontSize  = 1;
  constructor(
    private authService: AuthService,
    private navCtrl: NavController,
    private alertService: AlertService,
    private formBuilder: FormBuilder,
    private translateConfigService: TranslateConfigService


  ) {
    this.forgotForm = this.formBuilder.group({
      email    : ['', Validators.compose([Validators.required])],
    });
  }

  ionViewWillEnter() {
    this.subscribeObj1 = this.translateConfigService.selectedFontSize.subscribe(size => {
      this.selectedFontSize = size;
      document.body.style.setProperty('--pixels', this.selectedFontSize.toString());
    });

  }
  ionViewWillLeave() {
    this.subscribeObj1.unsubscribe();
  }
  ngOnInit() {
  }
  forgot(form) {

    this.messages = this.translateConfigService.getMessages('FORGOT.messages');
    this.authService.forgot(form.value.email).then(
      data => {
        this.userData = data;
        if (this.userData.status === 1) {
          this.navCtrl.navigateRoot('login');
        }
        this.alertService.presentToast(this.messages[this.userData.status]);
      },
      error => {
        console.log(error);
      },
    );
  }
}
