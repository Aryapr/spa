import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
// import { ModalController } from '@ionic/angular';

import { AuthService } from 'src/app/services/auth.service';
import { AlertService } from 'src/app/services/alert.service';

import { TranslateConfigService } from '../services/translate-config.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm: FormGroup;
  userData: any;
  messages;
  subscribeObj1;
  selectedFontSize  = 1;
  constructor(
    private authService: AuthService,
    private navCtrl: NavController,
    private alertService: AlertService,
    private formBuilder: FormBuilder,
    public translateConfigService: TranslateConfigService
  ) {

    this.loginForm = this.formBuilder.group({
      username    : ['', Validators.compose([Validators.required])],
      password : ['', Validators.compose([Validators.required])]
    });
  }

  ionViewWillLeave() {
    this.subscribeObj1.unsubscribe();
 }
  ionViewWillEnter() {
    this.authService.isLoggedIn.subscribe(state => {
      if (state) {
        this.navCtrl.navigateRoot('start/tabs/home');
      }
    });
    this.subscribeObj1 = this.translateConfigService.selectedFontSize.subscribe(size => {
      this.selectedFontSize = size;
      document.body.style.setProperty('--pixels', this.selectedFontSize.toString());
    });
  }

  ngOnInit() {
  }
  login(form) {


    this.messages = this.translateConfigService.getMessages('LOGIN.messages');

    console.log(this.messages);
    this.authService.login(form.value.username, form.value.password).then(
      data => {
        this.userData = data;
        this.alertService.presentToast(this.messages[this.userData.loggedin]);

        if (this.userData.loggedin === 1) {
          this.navCtrl.navigateRoot('start/tabs/home');
        }
      },
      error => {
        console.log(error);
      }
    );
  }
}
