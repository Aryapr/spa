import { Component, OnInit } from "@angular/core";
import { ContentService } from "../services/content.service";
import { LoadingController } from "@ionic/angular";
import { Router, ActivatedRoute } from "@angular/router";
import { TranslateConfigService } from "../services/translate-config.service";
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";
import { Storage } from "@ionic/storage";
@Component({
  selector: "app-resources",
  templateUrl: "./resources.page.html",
  styleUrls: ["./resources.page.scss"]
})
export class ResourcesPage implements OnInit {
  resourcesData = {
    ID: "",
    post_title: "Resources",
    post_content: "",
    banner_image: "../../assets/images/about.png",
    tab_info: [
      {
        tab_name: "",
        details: [
          {
            tab_title: "",
            tab_short_description: "",
            tab_url: ""
          }
        ]
      }
    ]
  };
  tabs: object = [
    { key: "taba", value: "tab1" },
    { key: "tabb", value: "tab2" }
  ];
  slideOpts = {
    initialSlide: 1,
    speed: 400
  };
  selectedLanguage: string;
  loading;
  subscribeObj;
  subscribeObj1;
  selectedFontSize = 1;
  nativeData1;
  nativeData;
  segment = "";
  noData = "";
  selectedProvince = "all";
  appLang = "";
  constructor(
    private content: ContentService,
    public loadingController: LoadingController,
    private translateConfigService: TranslateConfigService,
    private route: ActivatedRoute,
    private router: Router,
    private iab: InAppBrowser,
    private storage: Storage
  ) {}

  openBrowser(url) {
    console.log(url);
    // this.iab.create(url);
    //  const options:  = {zoom: 'no'};
    this.iab.create(url, "_system");
    //  window.open(url, '_system', 'location=yes');
  }

  ionViewDidEnter() {
    this.subscribeObj = this.translateConfigService.selectedLanguage.subscribe(
      lang => {
        this.appLang = lang;
        this.getContents();
      }
    );
    this.subscribeObj1 = this.translateConfigService.selectedFontSize.subscribe(
      size => {
        this.selectedFontSize = size;
        document.body.style.setProperty(
          "--pixels",
          this.selectedFontSize.toString()
        );
      }
    );
  }

  ionViewWillLeave() {
    this.subscribeObj.unsubscribe();
    this.subscribeObj1.unsubscribe();
    const vid: any = document.getElementById("videoId1");
    vid.pause();
  }

  async getContents() {
    this.noData = "";
    const loading = await this.loadingController.create({
      message: this.translateConfigService.getMessages("loading")
    });

    /** Get data from local storage */
    const page = "resources";
    const lang = this.translateConfigService.selected;
    const storageSlug = page + "_" + lang;
  //  const resourcesData = await this.storage.get(storageSlug);
    // if (this.resourcesData) {
    //   this.resourcesData = this.resourcesData[0];
    //   this.segment = this.resourcesData.tab_info[0].tab_name;
    // } else {
    //   this.resourcesData = {
    //     ID: "",
    //     post_title: "Resources",
    //     post_content: "",
    //     banner_image: "../../assets/images/about.png",
    //     tab_info: [
    //       {
    //         tab_name: "",
    //         details: [
    //           {
    //             tab_title: "",
    //             tab_short_description: "",
    //             tab_url: ""
    //           }
    //         ]
    //       }
    //     ]
    //   };
    // }

    // if (!this.resourcesData.ID) {
    //   await loading.present();
    // }
    await loading.present();
    // console.log(56565656, this.appLang);
    await this.content.getResContents(page, this.selectedProvince).then(
    // await this.content.getResContentsNew(page, this.selectedProvince).subscribe(
      data => {
        // Set the data to display in the template
        if (data) {
          this.resourcesData = data[0];
          this.segment = this.resourcesData.tab_info[0].tab_name;
        }
        loading.dismiss();
        this.noData = this.translateConfigService.getMessages("noData");
      },
      err => {
        console.log(err);
        loading.dismiss();
        this.noData = this.translateConfigService.getMessages("noData");
      }
    );
  }

  selectProvince(province) {
    this.selectedProvince = province;
    console.log(799, province);
    this.subscribeObj = this.translateConfigService.selectedLanguage.subscribe(
      lang => {
        // Load the data
        this.getContents();
      }
    );
  }

  ngOnInit() {}
}
