import { NgModule , CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ResourcesPageRoutingModule } from './resources-routing.module';

import { ResourcesPage } from './resources.page';
import { PipesModule } from '../pipes/pipe.module';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PipesModule,
    ResourcesPageRoutingModule,
    TranslateModule
  ],
  declarations: [ResourcesPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class ResourcesPageModule {}
