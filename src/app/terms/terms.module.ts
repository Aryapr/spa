import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TermsPageRoutingModule } from './terms-routing.module';

import { TermsPage } from './terms.page';

import { TranslateModule } from '@ngx-translate/core';
import { PipesModule } from '../pipes/pipe.module';

// import { MiAccordionComponent } from '../widgets/mi-accordion/mi-accordion.component';
import { LessonsPageModule } from './../lessons/lessons.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TermsPageRoutingModule,
    TranslateModule,
    PipesModule,
    LessonsPageModule
  ],
  declarations: [
    TermsPage,
    // MiAccordionComponent
  ],
 
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class TermsPageModule {}
