import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';

import { ContentService } from '../services/content.service';
import { TranslateConfigService } from '../services/translate-config.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.page.html',
  styleUrls: ['./terms.page.scss'],
})
export class TermsPage implements OnInit {

  data: any = [{
    terms : []
  }];
  selectedFontSize  = 1;
  noData = '';
  subscribeObj;
  subscribeObj1;
  constructor(
    private content: ContentService,
    public loadingController: LoadingController,
    private route: ActivatedRoute,
    private router: Router,
    private translateConfigService: TranslateConfigService,
    private storage: Storage,
  ) { }

  ionViewWillEnter() {
    // Load the data
    this.subscribeObj  = this.translateConfigService.selectedLanguage.subscribe(lang => {
      this.getContents();
    });
    this.subscribeObj1 = this.translateConfigService.selectedFontSize.subscribe(size => {
      this.selectedFontSize = size;
      document.body.style.setProperty('--pixels', this.selectedFontSize.toString());
    });

  }
  ionViewWillLeave() {
    this.subscribeObj.unsubscribe();
    this.subscribeObj1.unsubscribe();
  }
  ngOnInit() {

  }

  async getContents() {
    this.noData = '';
    const loading = await this.loadingController.create({
      message: this.translateConfigService.getMessages('loading')
    });


    const page = 'terms';
    const lang = this.translateConfigService.selected;
    const storageSlug   =   page + '_' + lang;
    // this.data = await this.storage.get(storageSlug);

    // if (!this.data) {
    //   await loading.present();
    // }
    await loading.present();
    await this.content.getContents(page)
      .then(
        data => {
          const lg = this.translateConfigService.selected;
          if (data) {
            const termsData = data;
            // if (termsData.lang === lg) {
            this.data = termsData;
            // }
          } else {
            this.data = '';
          }
          loading.dismiss();
          this.noData = this.translateConfigService.getMessages('noData');
        },
        err => {
          console.log(err);
          loading.dismiss();
          this.noData = this.translateConfigService.getMessages('noData');
        }
    );

  }


}
