import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WorkshopPageRoutingModule } from './workshop-routing.module';

import { WorkshopPage } from './workshop.page';
import { PipesModule } from '../pipes/pipe.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PipesModule,
    TranslateModule,
    WorkshopPageRoutingModule
  ],
  declarations: [WorkshopPage]
})
export class WorkshopPageModule {}
