import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { ContentService } from '../services/content.service';
import { TranslateConfigService } from '../services/translate-config.service';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-workshop',
  templateUrl: './workshop.page.html',
  styleUrls: ['./workshop.page.scss'],
})
export class WorkshopPage implements OnInit {

  workData = {
    workshops1: '',
    workshops2: '',
    lang: "",
    // banner_image: '../../assets/images/about.png',
    // organization_content: '',
    // button_name: 'Register'
  };
  selectedFontSize  = 1;
  noData = '';
  subscribeObj;
  subscribeObj1;
  trustedVideoUrl1: any;
  trustedVideoUrl2: any;
  curLang: any;
  trustedVideoUrl_en_1: any;
  trustedVideoUrl_ar_1: any;
  trustedVideoUrl_en_2: any;
  trustedVideoUrl_ar_2: any;
  constructor(
      private content: ContentService,
      public loadingController: LoadingController,
      private route: ActivatedRoute,
      private router: Router,
     private translateConfigService: TranslateConfigService,
     //private storage: Storage,
     public  sanitizer: DomSanitizer,

  ) { }

  ionViewWillEnter() {
    //Load the data
    this.subscribeObj  = this.translateConfigService.selectedLanguage.subscribe(lang => {
      //console.log("current lang >>",lang )
      this.curLang = lang
      this.getContents();
    });
    this.subscribeObj1 = this.translateConfigService.selectedFontSize.subscribe(size => {
      this.selectedFontSize = size;
      document.body.style.setProperty('--pixels', this.selectedFontSize.toString());
    });

  }
  ionViewWillLeave() {
    this.subscribeObj.unsubscribe();
    this.subscribeObj1.unsubscribe();
    const vid: any = document.getElementById('videoId3');
    vid.pause();
  }
  ngOnInit() {
  }

  async getContents() {
    this.noData = '';
    const loading = await this.loadingController.create({
      message: this.translateConfigService.getMessages('loading')
    });


    const page = 'workshops';
    const lang = this.translateConfigService.selected;
    const storageSlug   =   page + '_' + lang;
    // this.data = await this.storage.get(storageSlug);

    // if (!this.data) {
    //   await loading.present();
    // }
    await loading.present();
    await this.content.getContents(page)
      .then(
        data => {
          //console.log(data);
          const lg = this.translateConfigService.selected;
          if (data) {
            this.workData = data[0];
           // console.log(this.workData.lang, "langurl>>",this.workData.workshops1,this.workData.workshops2)

            this.trustedVideoUrl_en_1 =  this.workData['videos_1']['en'];
            this.trustedVideoUrl_ar_1 =  this.workData['videos_1']['ar'];
            
            this.trustedVideoUrl_en_2 =  this.workData['videos_2']['en'];
            this.trustedVideoUrl_ar_2 =  this.workData['videos_2']['ar'];

//console.log("urls>>",this.trustedVideoUrl1,">>",this.trustedVideoUrl2)

            // if (termsData.lang === lg) {
            //this.data = workData;
            // }
          }
          //  else {
          //   this.data = '';
          // }
          loading.dismiss();
          this.noData = this.translateConfigService.getMessages('noData');
        },
        err => {
          console.log(err);
          loading.dismiss();
          this.noData = this.translateConfigService.getMessages('noData');
        }
    );

  }


}
