import { Component, OnInit } from '@angular/core';
import { ModulesService } from '../services/modules.service';
import { Router, ActivatedRoute } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { TranslateConfigService } from '../services/translate-config.service';
import { AuthService } from 'src/app/services/auth.service';
import { Storage } from '@ionic/storage';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-lessons',
  templateUrl: './lessons.page.html',
  styleUrls: ['./lessons.page.scss'],
})
export class LessonsPage implements OnInit {
   lessonsInfo: any = '';
   moduleId: string;
   subscribeObj;
   subscribeObj1;
   selectedFontSize  = 1;
   trustedVideoUrl: any;
   noData = '';
   constructor(
      private module: ModulesService,
      private router: Router,
      private route: ActivatedRoute,
      private loadingController: LoadingController,
      private translateConfigService: TranslateConfigService,
      private authService: AuthService,
      private storage: Storage,
      public  sanitizer: DomSanitizer,
   ) {
   }

   ionViewWillEnter() {
      // Load the data
      this.subscribeObj =  this.translateConfigService.selectedLanguage.subscribe(lang => {
         this.getLessons();
      });
      this.subscribeObj1 = this.translateConfigService.selectedFontSize.subscribe(size => {
         this.selectedFontSize = size;
         document.body.style.setProperty('--pixels', this.selectedFontSize.toString());
       });
    }
   ionViewWillLeave() {
      this.subscribeObj.unsubscribe();
      this.subscribeObj1.unsubscribe();
      this.stopVideos();
   }
   stopVideos() {
      const videos = document.querySelectorAll('iframe, video');
      Array.prototype.forEach.call(videos, (video) => {

         // console.log('video', video, video.tagName.toLowerCase(), video.id);
         const id = video.id;
         if (video.tagName.toLowerCase() === 'video') {
            video.pause();
         } else if (id === 'videoId1' || id === 'videoId2') {
            const src = video.src;
            video.src = src;
         }
      });
   }
   ngOnInit() {

   }

   async getLessons() {
      this.noData = '';

      const loading = await this.loadingController.create({
         message: this.translateConfigService.getMessages('loading')
      });

      this.moduleId = this.route.snapshot.paramMap.get('mid');

      /** Get data from local storage */
      const lang  = this.translateConfigService.selected;
      const user = this.authService.userData;
      const userId  = user.user_info.ID;
      const storageSlug   =   'lessons' + '_' + lang + '_' + this.moduleId + '_' + userId;
   //   this.lessonsInfo = await this.storage.get(storageSlug);
      if (this.lessonsInfo) {
         this.trustedVideoUrl = this.lessonsInfo.video_url ?
                                 this.sanitizer.bypassSecurityTrustResourceUrl(this.lessonsInfo.video_url) :
                                 '' ;
         console.log('Local', this.trustedVideoUrl, this.lessonsInfo);
      }

      if (!this.lessonsInfo) {
         await loading.present();
      }

      await this.module.getLessons(this.moduleId)
         .then(res => {
            if (res) {
               this.lessonsInfo = res;
               this.trustedVideoUrl = this.lessonsInfo.video_url ?
                                       this.sanitizer.bypassSecurityTrustResourceUrl(this.lessonsInfo.video_url) :
                                       '';
               console.log('server', this.trustedVideoUrl, res);
            }
            loading.dismiss();
            this.noData = this.translateConfigService.getMessages('noData');
         }, err => {
         console.log(err);
         loading.dismiss();
         this.noData = this.translateConfigService.getMessages('noData');
         });

   }
  /**
   * Captures and console logs the value emitted from the user depressing the accordion component's <ion-button> element
   * public
   * method captureName
   * param {any}		event 				The captured event
   * returns {none}
   */
   public captureName(event: any): void {
      // console.log(`Captured name by event value: ${event}`);
   }

}
