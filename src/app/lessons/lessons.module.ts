import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LessonsPageRoutingModule } from './lessons-routing.module';

import { LessonsPage } from './lessons.page';

import { MiAccordionComponent } from '../widgets/mi-accordion/mi-accordion.component';

import { TranslateModule } from '@ngx-translate/core';
import { PipesModule } from '../pipes/pipe.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LessonsPageRoutingModule,
    TranslateModule,
    PipesModule
  ],
  exports: [
    MiAccordionComponent
  ],
  declarations: [LessonsPage, MiAccordionComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LessonsPageModule {}
