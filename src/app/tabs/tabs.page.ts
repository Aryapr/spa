import { Component, ViewChild, OnInit, OnDestroy  } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { TranslateConfigService } from '../services/translate-config.service';
import { LiveChatWidgetApiModel, LiveChatWidgetModel  } from '@livechat/angular-widget';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  isLoggedIn: boolean;
  visitor;
  params;
  public isLiveChatWidgetLoaded = false;
  appLang = '';
  subscribeObj2;
  // liveChatApi: LiveChatWidgetApiModel;
  // @ViewChild('liveChatWidget', {static: false}) public liveChatWidget: LiveChatWidgetModel;

  @ViewChild('liveChatWidget', {static: false}) liveChatWidget: LiveChatWidgetModel;
  liveChatWidget$: Subscription = new Subscription();
  liveChatApi: LiveChatWidgetApiModel;
  constructor(
    private authService: AuthService,
    private translateConfigService: TranslateConfigService
    ) {}

  onChatLoaded(api: LiveChatWidgetApiModel): void {
    this.liveChatApi = api;
    this.isLiveChatWidgetLoaded = true;
  }
  openChatWindow(): void {
    console.log('this.isLiveChatWidgetLoaded', this.isLiveChatWidgetLoaded);
    if (this.isLiveChatWidgetLoaded === true) {
    this.liveChatWidget.openChatWindow();
    } else {
    }
  }
  onChatWindowMinimized() {
    console.log('minimized');
  }

  onChatWindowOpened() {
    console.log('opened');
  }


  ionViewWillEnter() {
    this.authService.isLoggedIn.subscribe(state => {
      this.isLoggedIn = state;
    });

    this.liveChatWidget$ = this.liveChatWidget.onChatLoaded.subscribe((api: LiveChatWidgetApiModel) => {
      this.liveChatApi = api;
      this.isLiveChatWidgetLoaded = true;
    });
    this.subscribeObj2 = this.translateConfigService.selectedLanguage.subscribe(
      lang => {
        this.appLang = lang;
      }
    );
  }

  ionViewWillLeave() {
    this.liveChatWidget$.unsubscribe();
    this.subscribeObj2.unsubscribe();
  }

}
