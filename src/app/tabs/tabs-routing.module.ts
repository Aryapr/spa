import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';
import { AuthGuard } from '../guard/auth.guard';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'home',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../home/home.module').then(m => m.HomePageModule)
          }
        ]
      },
      {
        path: 'about',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../about/about.module').then(m => m.AboutPageModule)
          }
        ]
      },
      {
        path: 'contact',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../contact/contact.module').then(m => m.ContactPageModule)
          }
        ]
      },
      {
        path: 'workshop',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../workshop/workshop.module').then(m => m.WorkshopPageModule)
          }
        ]
      },
      {
        path: 'resources',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../resources/resources.module').then(m => m.ResourcesPageModule)
          }
        ]
      },
      {
        path: 'modules',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../modules/modules.module').then(m => m.ModulesPageModule)
          }
        ]
      },
      {
        path: 'modules/:mid',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../lessons/lessons.module').then(m => m.LessonsPageModule),
            canActivate: [AuthGuard]
          }
        ]
      },
      {
        path: 'lessons/:mid/:lid',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../lesson/lesson.module').then(m => m.LessonPageModule),
            canActivate: [AuthGuard]
          }
        ]
      },
      {
        path: 'quiz/:mid/:lid',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../quiz/quiz.module').then(m => m.QuizPageModule),
            canActivate: [AuthGuard]
          }
        ]
      },
      {
        path: 'map',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../map/map.module').then(m => m.MapPageModule)
          }
        ]
      },
      {
        path: 'map-explore',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../map-explore/map-explore.module').then(m => m.MapExplorePageModule)
          }
        ]
      },
      {
        path: 'map-data',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../map-data/map-data.module').then(m => m.MapDataPageModule)
          }
        ]
      },
      {
        path: 'my-account',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../my-account/my-account.module').then(m => m.MyAccountPageModule)
          }
        ],
        canActivate: [AuthGuard]
      },
      {
        path: 'login',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../login/login.module').then(m => m.LoginPageModule)
          }
        ]
      },
      { path: '', redirectTo: '/start/tabs/home', pathMatch: 'full'}
    ]
  },
  {
    path: '',
    redirectTo: '/start/tabs/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
