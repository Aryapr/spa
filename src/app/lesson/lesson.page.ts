import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { NavController } from '@ionic/angular';
import { ModulesService } from '../services/modules.service';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player/ngx';

import { ModalController } from '@ionic/angular';
import { ProvincePage } from '../province/province.page';

import {DomSanitizer} from '@angular/platform-browser';
import { TranslateConfigService } from '../services/translate-config.service';
import { File } from '@ionic-native/File/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';


import { Platform } from '@ionic/angular';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { Storage } from '@ionic/storage';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-lesson',
  templateUrl: './lesson.page.html',
  styleUrls: ['./lesson.page.scss'],
})
export class LessonPage implements OnInit {
  lessonId: any;
  moduleId: any;
  lastLesson: any;
  nextLesson: any;
  lessonInfo: any = '';
  trustedVideoUrl: any;
  subscribeObj: any;
  netStatus: any;
  trustedVideoUrlOffline: any;
  downloadProcessing = '';
  disableDownload = false;
  lessonData = {
    banner_image: '../../assets/images/lesson-offline-video.png',
  };
  noData = '';
  subscribeObj1: any;
  selectedFontSize  = 1;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private module: ModulesService,
    private loadingController: LoadingController,
    private modalController: ModalController,
    private navCtrl: NavController,
    public  sanitizer: DomSanitizer,
    private translateConfigService: TranslateConfigService,
    private youtube: YoutubeVideoPlayer,
    private transfer: FileTransfer,
    public file: File,
    private platform: Platform,
    private fileOpener: FileOpener,
    private authService: AuthService,
    private storage: Storage,
  ) {

  }

  ionViewWillEnter() {
    // Load the data
    this.subscribeObj = this.translateConfigService.selectedLanguage.subscribe(lang => {
      this.getLesson();
    });
    this.subscribeObj1 = this.translateConfigService.selectedFontSize.subscribe(size => {
      this.selectedFontSize = size;
      document.body.style.setProperty('--pixels', this.selectedFontSize.toString());
    });

  }

  ionViewWillLeave() {
    this.subscribeObj.unsubscribe();
    this.subscribeObj1.unsubscribe();
    const listaFrames = document.getElementsByTagName('iframe');
    const iframe = listaFrames[0].contentWindow;
    iframe.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
    this.stopVideos();
  }
  ngOnInit() {
    this.netStatus = this.translateConfigService.networkCheck();
  }

  async getLesson() {

    this.noData = '';
    this.moduleId = this.route.snapshot.paramMap.get('mid');
    this.lessonId = this.route.snapshot.paramMap.get('lid');

    const loading = await this.loadingController.create({
      message: this.translateConfigService.getMessages('loading')
    });

    /** Get data from local storage */
    const lang  = this.translateConfigService.selected;
    const user = this.authService.userData;
    const userId  = user.user_info.ID;
    const storageSlug   =   'lesson' + '_' + lang + '_' + this.lessonId + '_' + this.moduleId + '_' + userId;
   // this.lessonInfo = await this.storage.get(storageSlug);
    if (this.lessonInfo) {
      this.trustedVideoUrl = this.lessonInfo.video_url ?
      this.sanitizer.bypassSecurityTrustResourceUrl(this.lessonInfo.video_url) : '';

      console.log('Local', this.trustedVideoUrl, this.lessonInfo);
    }
    if (this.lessonInfo) {
      this.lastLesson = this.lessonInfo.lastLesson;
      this.nextLesson = this.lessonInfo.nextLesson;
    }
    if (!this.lessonInfo) {
      await loading.present();
    }
    await this.module.getLesson( this.moduleId, this.lessonId ).then (
      data => {
        if (data) {
          this.lessonInfo = data;
          this.lastLesson = this.lessonInfo.lastLesson;
          this.nextLesson = this.lessonInfo.nextLesson;
          this.trustedVideoUrl = this.lessonInfo.video_url ?
                                  this.sanitizer.bypassSecurityTrustResourceUrl(this.lessonInfo.video_url) :
                                  this.lessonInfo.video_url ;
          this.netStatus = this.translateConfigService.networkCheck();
          console.log('server', this.trustedVideoUrl, data);
          /* if (this.netStatus === 'error') {
          const filename = this.lessonInfo.storageSlug + '.' + this.lessonInfo.download_video_ext;
          this.file.readAsDataURL(this.file.dataDirectory, filename ).then((base64) => {
              this.trustedVideoUrlOffline = this.sanitizer.bypassSecurityTrustUrl(base64);
              console.log(this.trustedVideoUrlOffline);
          }).catch((err) => {
              console.log('VIDEO :: No se pudo recuperar el video');
              console.log(err);
          });
          }*/
        }
        loading.dismiss();
        this.noData = this.translateConfigService.getMessages('noData');
      }, error => {
        console.log(error);
        loading.dismiss();
        this.noData = this.translateConfigService.getMessages('noData');
      }
    );
  }

  async download(type) {


    this.downloadProcessing = this.translateConfigService.getMessages('LESSON.processingDownload');
    this.disableDownload  = true;

    const downloadUrl = (type === '1' ? this.lessonInfo.download_video_url : this.lessonInfo.download_low_quality_video_url);
    const path = this.file.dataDirectory;
    const transfer = this.transfer.create();

    const filename = (type === '1' ? (this.lessonInfo.storageSlug + '.' + this.lessonInfo.download_video_ext) :
    (this.lessonInfo.storageSlug + '.' + this.lessonInfo.download_low_quality_video_ext));
    // const filename = this.lessonInfo.storageSlug + '.' + this.lessonInfo.download_video_ext;
    transfer.download(downloadUrl, path + filename ).then(entry => {

      this.downloadProcessing = '';
      this.disableDownload  = false;
      const url = entry.toURL();
      this.trustedVideoUrlOffline = url;
      this.translateConfigService.presentAlerts(
        this.translateConfigService.getMessages('LESSON.lessonVideo'),
        this.translateConfigService.getMessages('LESSON.downloadComplete')
      );
      this.storage.set(this.lessonInfo.storageSlug, url).then(
        () => {
          console.log('data  Stored');
          console.log(url);
          this.moduleId = this.route.snapshot.paramMap.get('mid');
          this.lessonId = this.route.snapshot.paramMap.get('lid');
          this.module.saveDownloadStatus( this.moduleId, this.lessonId ).then (
            data => {}, error => {}
          );

        },
        error => console.error('Error storing user info', error)
      );
    });
  }
  playLocalVideo() {

    // if (this.platform.is('ios')) {
    //   // this.document.viewDocument(url, 'application/pdf', {});
    // } else {
      this.storage.get(this.lessonInfo.storageSlug).then(
        (url) => {
          if (url ) {
            const path = this.file.dataDirectory;
            const filename = this.lessonInfo.storageSlug + '.' + this.lessonInfo.download_video_ext;
            this.fileOpener.open(url, 'video/mp4')
              .then(() => {console.log('File is opened'); })
              .catch(e => {
                this.translateConfigService.presentAlerts(
                  this.translateConfigService.getMessages('LESSON.lessonVideo'),
                  this.translateConfigService.getMessages('LESSON.openError')
                );
                console.log('Error opening file', e);
              });
          } else {
            this.translateConfigService.presentAlerts( 
              this.translateConfigService.getMessages('LESSON.lessonVideo'),
              this.translateConfigService.getMessages('LESSON.urlMissing')
            );
            console.error('Error getting url info');
          }
          },
        error => {
          // tslint:disable-next-line:max-line-length
          this.translateConfigService.presentAlerts( 
            this.translateConfigService.getMessages('LESSON.lessonVideo'),
            this.translateConfigService.getMessages('LESSON.urlMissing')
          );
          console.error('Error getting url info', error);
        }
      );

    // }
  }

  async presentModal(province, img) {
    const modal = await this.modalController.create({
      component: ProvincePage,
      componentProps: {
        province,
        img,
      }
    });
    return await modal.present();
  }

  goBack() {
    // this.navCtrl.('/start/tabs/modules/' + this.moduleId);
  }

  stopVideos() {
    const videos = document.querySelectorAll('iframe, video');
    Array.prototype.forEach.call(videos, (video) => {

      // console.log('video', video, video.tagName.toLowerCase(), video.id);
      const id = video.id;
      if (video.tagName.toLowerCase() === 'video') {
        video.pause();
      } else if (id === 'videoId1' || id === 'videoId2') {
        const src = video.src;
        video.src = src;
      }
    });
  }
}
