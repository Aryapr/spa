import { NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LessonPageRoutingModule } from './lesson-routing.module';

import { LessonPage } from './lesson.page';
import { ProvincePage } from '../province/province.page';
import { PipesModule } from '../pipes/pipe.module';


import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LessonPageRoutingModule,
    PipesModule,
    TranslateModule
  ],
  declarations: [LessonPage, ProvincePage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  entryComponents: [ProvincePage]
})
export class LessonPageModule {}
