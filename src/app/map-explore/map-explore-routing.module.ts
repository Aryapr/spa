import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MapExplorePage } from './map-explore.page';

const routes: Routes = [
  {
    path: '',
    component: MapExplorePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MapExplorePageRoutingModule {}
