import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MapExplorePage } from './map-explore.page';

describe('MapExplorePage', () => {
  let component: MapExplorePage;
  let fixture: ComponentFixture<MapExplorePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapExplorePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MapExplorePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
