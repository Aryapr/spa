import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MapExplorePageRoutingModule } from './map-explore-routing.module';

import { MapExplorePage } from './map-explore.page';
import { PipesModule } from '../pipes/pipe.module';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MapExplorePageRoutingModule,
    PipesModule,
    TranslateModule
  ],
  declarations: [MapExplorePage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MapExplorePageModule {}
