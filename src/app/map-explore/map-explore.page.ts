import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { ContentService } from '../services/content.service';
import { Router, NavigationExtras } from '@angular/router';
import { TranslateConfigService } from '../services/translate-config.service';
import { Storage } from '@ionic/storage'; 
@Component({
  selector: 'app-map-explore',
  templateUrl: './map-explore.page.html',
  styleUrls: ['./map-explore.page.scss'],
})
export class MapExplorePage implements OnInit {

  mapData: any;
  data: any;
  checked = false;
  selectedIds = [];
  subscribeObj;
  subscribeObj1;
  selectedFontSize  = 1;
  noData = '';
  constructor(
    private content: ContentService,
    public loadingController: LoadingController,
    private router: Router,
    private translateConfigService: TranslateConfigService,
    private storage: Storage,
    ) {

      this.selectedIds = [];

   }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.subscribeObj  = this.translateConfigService.selectedLanguage.subscribe(lang => {
      this.getContentsMap();
    });
    this.subscribeObj1 = this.translateConfigService.selectedFontSize.subscribe(size => {
      this.selectedFontSize = size;
      document.body.style.setProperty('--pixels', this.selectedFontSize.toString());
    });
  }
  ionViewWillLeave() {
    this.subscribeObj.unsubscribe();
    this.subscribeObj1.unsubscribe();
  }
  getContents(id, state) {

    if (this.selectedIds.indexOf(id) === -1) {
      this.selectedIds.push(id);
    }
    if (this.checked && this.selectedIds.length < 2) {
      return false;
    }
    const navigationExtras: NavigationExtras = {
      state: {
        ids: JSON.stringify(this.selectedIds),
        compareStatus: this.checked,
        state
      }
    };
    this.router.navigate(['map-data'], navigationExtras);
    this.selectedIds = [];

  }
  async getContentsMap() {
    this.noData = '';
    const loading = await this.loadingController.create({
      message: this.translateConfigService.getMessages('loading')
    });

    /** Get data from local storage */
    const page = 'map';
    const lang = this.translateConfigService.selected;
    const storageSlug   =   page + '_' + lang;
  //  this.mapData = await this.storage.get(storageSlug);

    if (!this.mapData) {
      await loading.present();
    }
    await this.content.getContents(page)
      .then(
        data => {

          // Set the data to display in the template
          if (data) {
            this.mapData = data;
          }
          loading.dismiss();
          this.noData = this.translateConfigService.getMessages('noData');
        },
        err => {
          console.log(err);
          loading.dismiss();
          this.noData = this.translateConfigService.getMessages('noData');
        }
    );
  }
  addValue(): void {
    this.checked = !this.checked;
    this.selectedIds = [];
  }
}
