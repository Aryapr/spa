import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';

import { AuthService } from 'src/app/services/auth.service';
import { AlertService } from 'src/app/services/alert.service';

import { Platform } from '@ionic/angular';
import { File } from '@ionic-native/File/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer/ngx';

import { ModulesService } from '../services/modules.service';
import { TranslateConfigService } from '../services/translate-config.service';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.page.html',
  styleUrls: ['./my-account.page.scss'],
})
export class MyAccountPage implements OnInit {

  myaccountForm: FormGroup;
  userData: any;
  myDate = new Date();

  datePickerObj: any = {
    inputDate: new Date(), // default new Date()
    fromDate: new Date('1890-01-01'), // default null
    toDate: new Date(), // default null
    showTodayButton: false, // default true
    closeOnSelect: true, // default false
    mondayFirst: true, // default false
    setLabel: 'Set',  // default 'Set'
    todayLabel: 'Today', // default 'Today'
    closeLabel: 'Close', // default 'Close'
    titleLabel: 'Select a Date', // default null
    monthsList: ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
    weeksList: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
    dateFormat: 'YYYY-MM-DD', // default DD MMM YYYY
    clearButton : false , // default true
    momentLocale: 'pt-BR', // Default 'en-US'
    yearInAscending: true, // Default false
    btnCloseSetInReverse: true, // Default false
    btnProperties: {
      expand: 'block', // Default 'block'
      fill: '', // Default 'solid'
      size: '', // Default 'default'
      disabled: '', // Default false
      strong: '', // Default false
      color: '' // Default ''
    },
    arrowNextPrev: {
      nextArrowSrc: 'assets/images/arrow_right.svg',
      prevArrowSrc: 'assets/images/arrow_left.svg'
    }, // This object supports only SVG files.
    isSundayHighlighted : {
    fontColor: '#ee88bf' // Default null
    } // Default {}
  };
  passwordcheck: string;
  value: number;
  userInfo: any;
  user: any;
  userId: any;
  isMenuOpen = false;
  moduleid: any;
  certificateInfo: any;
  certResponse: any;
  subscribeObj;
  subscribeObj1;
  segment = 'user';
  noData: any;
  selectedFontSize  = 1;
  // fileTransfer: FileTransferObject;
  constructor(
    private authService: AuthService,
    private navCtrl: NavController,
    private alertService: AlertService,
    private formBuilder: FormBuilder,
    private loadingController: LoadingController,
    private platform: Platform,
    private file: File,
    private ft: FileTransfer,
    private fileOpener: FileOpener,
    private document: DocumentViewer,
    private module: ModulesService,
    private translateConfigService: TranslateConfigService,
    private storage: Storage,
   //  private fileOpener: FileOpener
   //  private transfer: FileTransfer,
   //  private file: File
  ) {

    this.myaccountForm = this.formBuilder.group({
      userid: [''],
      first_name: ['', Validators.compose([Validators.required])],
      last_name : ['', Validators.compose([Validators.required])],
      uci_number : ['', Validators.compose([Validators.required])],
      dob : ['', Validators.compose([Validators.required])],
      email : ['', Validators.compose([Validators.required])],
      pass_check : [false],
      password : [''],
      // new_password : [''],
      // confirm_password : [''],
      new_password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(5)
      ])),
      confirm_password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(5)
      ]))
    },
    {
      validators: this.password.bind(this)
    }
    );
  }

  ionViewWillEnter() {
    this.subscribeObj = this.translateConfigService.selectedLanguage.subscribe(lang => {
      // Load the data
      this.getUserdetails();
    });
    this.subscribeObj1 = this.translateConfigService.selectedFontSize.subscribe(size => {
      this.selectedFontSize = size;
      document.body.style.setProperty('--pixels', this.selectedFontSize.toString());
    });
  }
  ionViewWillLeave() {
    this.subscribeObj.unsubscribe();
    this.subscribeObj1.unsubscribe();
  }

  ngOnInit() {

  }

  passwordCheck(): void {

    console.log(this.myaccountForm.value);
    this.isMenuOpen = !this.isMenuOpen;
}

  password(formGroup: FormGroup) {
    // tslint:disable-next-line:variable-name
    const { value: new_password } = formGroup.get('new_password');
    // tslint:disable-next-line:variable-name
    const { value: confirm_password } = formGroup.get('confirm_password');
    return new_password === confirm_password ? null : { passwordNotMatch: true };
  }
  async getUserdetails() {

    this.noData = '';
    this.user = this.authService.userData;
    this.userId  = this.user.user_info.ID;
    const loading = await this.loadingController.create({
      message: this.translateConfigService.getMessages('loading')
    });

    /** Get data from local storage */
    const lang = this.translateConfigService.selected;
    const storageSlug   =   'getAccountInfo' + '_' + lang + '_' + this.userId;
  //  this.userInfo = await this.storage.get(storageSlug);
    if (!this.userInfo) {
      await loading.present();
    }
    await this.authService.getAccountInfoNew( this.userId ).subscribe (
      data => {
        if (data) {
          this.userInfo = data;
        }
        this.isMenuOpen = false;
        this.myaccountForm.setValue({
          userid: this.userId,
          first_name: this.userInfo.firstname,
          last_name : this.userInfo.lastname,
          uci_number : this.userInfo.uci_number,
          dob : this.userInfo.date_of_birth,
          email : this.userInfo.useremail,
          pass_check : false,
          password : '',
          new_password: '',
          confirm_password: ''
        },
        );
        this.noData = this.translateConfigService.getMessages('noData');
        loading.dismiss();
      }, error => {
        console.log(error);
        loading.dismiss();
      }
    );

  }
  async myaccountSave(form) {

    const loading = await this.loadingController.create({
      message: this.translateConfigService.getMessages('PleaseWait')
    });
    await loading.present();
    this.user = this.authService.userData;
    this.userId  = this.user.user_info.ID;
    this.authService.myaccountSave(this.userId, form.value.first_name, form.value.last_name,
      form.value.uci_number, form.value.dob, form.value.pass_check, form.value.password, form.value.new_password, 
      form.value.confirm_password).then(
      data => {
        this.userData = data;
        console.log(this.userData);
        if (this.userData.status === 1) {
          this.navCtrl.navigateRoot('start/tabs/my-account');
          this.getUserdetails();
        }
        this.alertService.presentToast(this.userData.message);
        loading.dismiss();
      },

      error => {
        loading.dismiss();
        console.log(error);
      }
    );
  }

  async download(fileName, moduleName) {
    const loading = await this.loadingController.create({
      message: this.translateConfigService.getMessages('generatingCertificate')
    });
    await loading.present();
    const downloadUrl = fileName;
    const path = this.file.dataDirectory;
    const transfer = this.ft.create();
    console.log(path);
    transfer.download(downloadUrl, path + moduleName + '.pdf').then(entry => {
      loading.dismiss();
      const url = entry.toURL();
      if (this.platform.is('ios')) {
        this.document.viewDocument(url, 'application/pdf', {});
      } else {
        this.fileOpener.open(url, 'application/pdf')
          .then(() => console.log('File is opened'))
          .catch(e => console.log('Error opening file', e));
      }
    });
  }

  async getCertificate(event) {
    this.moduleid = event.srcElement.id;
    const loading = await this.loadingController.create({
      message: this.translateConfigService.getMessages('loading')
    });
    await loading.present();
    this.module.getCertificate(this.moduleid).then(data => {
      this.certResponse = data;
      this.download( this.certResponse.fileName, this.certResponse.moduleName);
      loading.dismiss();
    }, error => {
      console.log(error);
      loading.dismiss();
    });
  }
}
