import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule , ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Ionic4DatepickerModule } from '@logisticinfotech/ionic4-datepicker';

import { MyAccountPageRoutingModule } from './my-account-routing.module';

import { MyAccountPage } from './my-account.page';

import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyAccountPageRoutingModule,
    ReactiveFormsModule,
    Ionic4DatepickerModule,
    TranslateModule
  ],
  declarations: [MyAccountPage]
})
export class MyAccountPageModule {}
