import { Component, OnInit, Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ModulesService } from '../services/modules.service';
import { TranslateConfigService } from '../services/translate-config.service';

import { Platform } from '@ionic/angular';
import { File } from '@ionic-native/File/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer/ngx';
import { Storage } from '@ionic/storage';
import { AuthService } from 'src/app/services/auth.service';
@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.page.html',
  styleUrls: ['./quiz.page.scss'],
})
export class QuizPage implements OnInit {
  lessonId;
  moduleId;
  quizInfo;
  lastLesson: any;
  nextLesson: any;
  quizForm: FormGroup;
  quizResponse;
  right = 0;
  total = 0;
  msg: any;
  msgCls = '';
  quizResponseStatus = false;

  alphabets = [];
  certResponse: any;
  messages;
  subscribeObj;
  noData = '';
  subscribeObj1;
  selectedFontSize  = 1;
  constructor(
    private loadingController: LoadingController,
    private route: ActivatedRoute,
    private router: Router,
    private module: ModulesService,
    private formBuilder: FormBuilder,
    private platform: Platform,
    private file: File,
    private ft: FileTransfer,
    private fileOpener: FileOpener,
    private document: DocumentViewer,
    private translateConfigService: TranslateConfigService,
    private authService: AuthService,
    private storage: Storage,
  ) {
    this.moduleId = this.route.snapshot.paramMap.get('mid');
    this.lessonId = this.route.snapshot.paramMap.get('lid');
    this.quizForm = this.formBuilder.group({});

    for ( let j = 65; j <= 90; j++ ) {
      this.alphabets.push(String.fromCharCode( j ));
    }
  }
  ionViewWillEnter() {
    // Load the data
    this.subscribeObj = this.translateConfigService.selectedLanguage.subscribe(lang => {
      this.getQuiz();
    });
    this.subscribeObj1 = this.translateConfigService.selectedFontSize.subscribe(size => {
      this.selectedFontSize = size;
      document.body.style.setProperty('--pixels', this.selectedFontSize.toString());
    });
  }
  ionViewWillLeave() {
    this.subscribeObj.unsubscribe();
    this.subscribeObj1.unsubscribe();
  }
  ngOnInit() {
  }

  async getQuiz() {

    this.noData = '';
    const loading = await this.loadingController.create({
      message: this.translateConfigService.getMessages('loading')
    });

    /** Get data from local storage */
    const lang  = this.translateConfigService.selected;
    const user = this.authService.userData;
    const userId  = user.user_info.ID;
    const storageSlug   =   'quiz' + '_' + lang + '_' + this.lessonId + '_' + this.moduleId + '_' + userId;
  //  this.quizInfo = await this.storage.get(storageSlug);
    if (this.quizInfo) {
      this.lastLesson = this.quizInfo.lastLesson;
      this.nextLesson = this.quizInfo.nextLesson;
    }

    if (!this.quizInfo) {
      await loading.present();
    }
    this.module.getQuiz(this.moduleId, this.lessonId).then(data => {

      if (data) {
        this.quizInfo = data;
        this.lastLesson = this.quizInfo.lastLesson;
        this.nextLesson = this.quizInfo.nextLesson;
        if (this.quizInfo) {
          if (this.quizInfo.questions) {
            for (const key in this.quizInfo.questions) {
              if (this.quizInfo.questions.hasOwnProperty(key)) {
                this.quizForm.addControl('question_' + key, new FormControl(''));
              }
            }
          }
        }
      } else {
        this.quizInfo = '';
      }
      loading.dismiss();
      this.noData = this.translateConfigService.getMessages('noData');
    }, error => {
      console.log(error);
      loading.dismiss();
      this.noData = this.translateConfigService.getMessages('noData');
    });
  }
  async submitQuiz(form) {

    this.messages = this.translateConfigService.getMessages('QUIZ.messages');
    const loading = await this.loadingController.create({
      message: this.translateConfigService.getMessages('loading')
    });
    await loading.present();
    this.module.submitQuiz(form.value, this.moduleId, this.lessonId).then(data => {
      this.quizResponseStatus = true;
      this.quizResponse = data;
      this.right  = this.quizResponse.right;
      this.total  = this.quizResponse.total;
      this.msg =  (this.quizResponse.status === 1) ?
      this.messages[1] :
      this.messages[0];
      this.msgCls = (this.quizResponse.status === 1) ? 'success' : 'error';
      loading.dismiss();
    }, error => {
      console.log(error);
      loading.dismiss();
    });
  }
  async getCertificate() {
    
    const loading = await this.loadingController.create({
      message: this.translateConfigService.getMessages('loading')
    });
    await loading.present();
    this.module.getCertificate(this.moduleId).then(data => {
      this.certResponse = data;
      this.download( this.certResponse.fileName, this.certResponse.moduleName);
      loading.dismiss();
    }, error => {
      console.log(error);
      loading.dismiss();
    });
  }
  backToQuiz() {
    this.quizResponseStatus =  false;
  }

  async download(fileName, moduleName) {
    const loading = await this.loadingController.create({
      message: this.translateConfigService.getMessages('generatingCertificate')
    });
    await loading.present();
    const downloadUrl = fileName;
    const path = this.file.dataDirectory;
    const transfer = this.ft.create();
    transfer.download(downloadUrl, path + moduleName + '.pdf').then(entry => {
      loading.dismiss();
      const url = entry.toURL();
      if (this.platform.is('ios')) {
        this.document.viewDocument(url, 'application/pdf', {});
      } else {
        this.fileOpener.open(url, 'application/pdf')
          .then(() => console.log('File is opened'))
          .catch(e => console.log('Error opening file', e));
      }
    });
  }

}
