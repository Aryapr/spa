import { Component, ViewChildren, ViewChild  , QueryList} from '@angular/core';

import { IonRouterOutlet, Platform, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Storage } from '@ionic/storage';
import { NavController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { TranslateConfigService } from './services/translate-config.service';
import { NetworkServiceService } from './services/network-service.service';
import { AlertService } from 'src/app/services/alert.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { LiveChatWidgetModel, LiveChatWidgetApiModel } from '@livechat/angular-widget';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  // lastTimeBackPress = 0;
  // timePeriodToExit = 2000;

  // @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;

  selected = '';
  public isLiveChatWidgetLoaded: boolean = false;
  public liveChatApi: LiveChatWidgetApiModel;
  @ViewChild('liveChatWidget', {static: false}) public liveChatWidget: LiveChatWidgetModel;

  public appPagesWithSubmenu = [
    {
      title: 'language',
      url: '',
      icon: '../../assets/images/language.svg',
      submenu: [
        {
          title: 'Arabic / العربية‏',
          url: '',
          value: 'ar'
        },
        {
          title: 'English',
          url: '',
          value: 'en'
        },
        {
          title: 'French / Français',
          url: '',
          value: 'fr'
        },
        // {
        //   title: 'Somali',
        //   url: '',
        //   value: 'so'
        // },
        {
          title: 'Farsi / فارسی',
          url: '',
          value: 'fa'
        },
        // {
        //   title: 'Kurdish',
        //   url: '',
        //   value: 'ku'
        // },
        {
          title: 'Dari / من',
          url: '',
          value: 'di'
        },

      ]
    }
  ];
  public commonMenus = [
    {
      title: 'aboutUs',
      url: '/start/tabs/about',
      icon: '../../assets/images/about-menu.svg', 
    },
    {
      title: 'Workshops',
      url: '/start/tabs/workshop',
      icon: '../../assets/images/accept.svg',
    },
    {
      title: 'contactUs',
      url: '/start/tabs/contact',
      icon: '../../assets/images/contact-menu.svg',
    },
    {
      title: 'resources',
      url: '/start/tabs/resources',
      icon: '../../assets/images/res-menu.svg',
    },
    {
      title: 'terms',
      url: '/terms',
      icon: '../../assets/images/accept.svg',
    },
  ];
  public appPagesSignIn = [
    {
      title: 'changePassword',
      url: '/start/tabs/my-account',
      icon: '../../assets/images/padlock.svg',
    },
    {
      title: 'signOut',
      url: '',
      icon: '../../assets/images/logout.svg',
      click: 'logout'

    }
  ];
  public appPagesSignout = [
    {
      title: 'signUp',
      url: '/register',
      icon: '../../assets/images/external-link-symbol.svg',
    },
    {
      title: 'signIn',
      url: '/start/tabs/login',
      icon: '../../assets/images/login.svg',
    },
  ];
  isLoggedIn: boolean;
  showSubmenu = false;
  subscribeObj;
  activePage;
  public counter = 0;
  visitor;
  params;
  text = 0;
  @ViewChild(IonRouterOutlet , { static: false }) routerOutlet: IonRouterOutlet;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private storage: NativeStorage,
    private istorage: Storage,
    private navCtrl: NavController,
    private authService: AuthService,
    private translateConfigService: TranslateConfigService,
    private networkServiceService: NetworkServiceService,
    private router: Router,
    private alertController: AlertController,
    public toastCtrl: ToastController,
    private alertService: AlertService
  ) {
    this.initializeApp();
    this.translateConfigService.networkCheck();
    // this.backButton();
    this.platform.backButton.subscribeWithPriority(0, () => {

      if (this.counter === 0 && this.router.url === '/') {
        this.counter++;
        this.alertService.presentToast(this.translateConfigService.getMessages('exitMsg'));
        setTimeout(() => { this.counter = 0; }, 3000);
      } else {
        // console.log("exitapp");
        navigator['app'].exitApp();
      }
    });

  }
  logout() {
    this.authService.logout();
  }

  setActivePage(page) {
    if ( page === 'changePassword' ) {
      this.activePage = page;
    } else {
      this.activePage = '';
    }
  }

  menuItemHandler(): void {
    this.showSubmenu = !this.showSubmenu;
    this.activePage = '';
  }
  updateFont() {
    this.translateConfigService.setFontSize(this.text);
  }
  setLanguage(value) {
    this.translateConfigService.setLanguage(value);
  }
  goToPage() {
    this.navCtrl.navigateRoot('start/tabs/home');
  }
  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.showSubmenu  = false;
      this.translateConfigService.setInitialAppLanguage();
      this.subscribeObj = this.translateConfigService.selectedLanguage.subscribe(value => {
        this.selected = value;
      });
      this.selected = this.translateConfigService.selected;
      console.log(this.selected);
      this.authService.isLoggedIn.subscribe(state => {
        this.isLoggedIn = state;
      });

    });
  }


  onChatLoaded(api: LiveChatWidgetApiModel): void {
    this.liveChatApi = api;
    this.isLiveChatWidgetLoaded = true;

    // Sometimes it can happen that LC_Invite is is still being loaded when onChatLoaded is called. To ensure that LC_Invite is loaded you can give additional check to onChatLoaded function:
    // api.on_after_load = () => {
    //   this.liveChatApi = api;
    //   this.isLiveChatWidgetLoaded = true;
    // };
  }

  onChatWindowMinimized() {
    console.log('minimized')
  }

  onChatWindowOpened() {
    console.log('opened')
  }



}


