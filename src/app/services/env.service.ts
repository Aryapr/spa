import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EnvService {
//  API_URL = 'https://estartsuccess.staging.wpengine.com/wp-json/api/v1/';
   API_URL = 'https://estartsuccess.ca/wp-json/api/v1/';
 //   API_URL = 'https://estartsuccess.ca/staging/wp-json/api/v1/';/
  constructor()  { }
}
