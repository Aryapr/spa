import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { EnvService } from './env.service';
import { AuthService } from 'src/app/services/auth.service';
import { Observable } from 'rxjs';
import { TranslateConfigService } from './translate-config.service';

import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class ModulesService {

  user: any;
  data:any;
  constructor(
    private http: HttpClient,
    private env: EnvService,
    private authService: AuthService,
    private translateConfigService: TranslateConfigService,
    private storage: Storage
  ) {
  }

  getModules() {

    let res;
    const lang  = this.translateConfigService.selected;
    const netWorkStatus =  this.translateConfigService.networkCheck();
    
    let userId = 0;
    const currentUser = this.authService.isAuthenticated();
    if (currentUser) {
      this.user = this.authService.userData;
      userId  = this.user.user_info.ID;
    }
    const storageSlug   =   'module' + '_' + lang;
    if ( netWorkStatus === 'success') {
      res = this.translateConfigService.httpPost(this.env.API_URL + 'modules', {lang, userId}, storageSlug);
    } else {
      res = this.translateConfigService.getDatafromLocalStorage ( storageSlug );
    }
    return res;
  }
  getModulesNew() {

    const lang  = this.translateConfigService.selected;
    const netWorkStatus =  this.translateConfigService.networkCheck();

    let userId = 0;
    const currentUser = this.authService.isAuthenticated();
    if (currentUser) {
      this.user = this.authService.userData;
      userId  = this.user.user_info.ID;
    }
    return this.http.post(this.env.API_URL + 'modules',
    {lang, userId}
    ).pipe(
      tap(res => {
        const storageSlug   =   'module' + '_' + lang;
        if ( netWorkStatus === 'success') {
          this.storage.set(storageSlug, res);
          this.data = res;
        } else {
          this.data = this.storage.get(storageSlug);
          this.storage.get(storageSlug).then((val) => {
          }, err => {
            this.translateConfigService.presentAlert();
          });
        }
      }),
    );
  }

  getLessons( id ) {

    let res;
    const netWorkStatus = this.translateConfigService.networkCheck();
    const lang  = this.translateConfigService.selected;
    this.user = this.authService.userData;
    const userId  = this.user.user_info.ID;
    const storageSlug   =   'lessons' + '_' + lang + '_' + id + '_' + userId;
    if ( netWorkStatus === 'success') {
      res = this.translateConfigService.httpPost(this.env.API_URL + 'lessons', { id, userId, lang }, storageSlug);
    } else {
      res = this.translateConfigService.getDatafromLocalStorage ( storageSlug );
    }
    return res;
  }

  getLessonsNew( id ) {

    const netWorkStatus = this.translateConfigService.networkCheck();
    const lang  = this.translateConfigService.selected;
    this.user = this.authService.userData;
    const userId  = this.user.user_info.ID;
    return this.http.post(this.env.API_URL + 'lessons',
    { id, userId, lang }
    ).pipe(
      tap(res => {
        const storageSlug   =   'lessons' + '_' + lang + '_' + id + '_' + userId;
        if ( netWorkStatus === 'success') {
          this.storage.set(storageSlug, res);
          this.data = res;
        } else {
          this.data = this.storage.get(storageSlug);
          this.storage.get(storageSlug).then((val) => {
          }, err => {
            this.translateConfigService.presentAlert();
          });
        }
      }),
    );
  }
  getLesson( moduleId, id ) {

    let res;
    const netWorkStatus = this.translateConfigService.networkCheck();
    const lang  = this.translateConfigService.selected;
    this.user = this.authService.userData;
    const userId  = this.user.user_info.ID;
    const storageSlug   =   'lesson' + '_' + lang + '_' + id + '_' + moduleId + '_' + userId;
    if ( netWorkStatus === 'success') {
      res = this.translateConfigService.httpPost(this.env.API_URL + 'lesson', { id, moduleId, userId, lang }, storageSlug);
    } else {
      res = this.translateConfigService.getDatafromLocalStorage ( storageSlug );
    }
    return res;
  }
  saveDownloadStatus(moduleId, lessonId) {

    const netWorkStatus = this.translateConfigService.networkCheck();
    this.user = this.authService.userData;
    const userId  = this.user.user_info.ID;
    if ( netWorkStatus === 'error') {
      this.translateConfigService.presentAlert();
    }
    return this.translateConfigService.httpPost(this.env.API_URL + 'saveDownloadStatus', { userId, moduleId, lessonId}, '');
  }

  saveDownloadStatusNew(moduleId, lessonId) {

    let res;
    const netWorkStatus = this.translateConfigService.networkCheck();
    const lang  = this.translateConfigService.selected;
    this.user = this.authService.userData;
    const userId  = this.user.user_info.ID;

    return this.http.post(this.env.API_URL + 'saveDownloadStatus',
    { userId, moduleId, lessonId }
    ).pipe(
      tap(res => {
      }),
    );

  }

  getLessonNew(  moduleId, id  ) {

    const netWorkStatus = this.translateConfigService.networkCheck();
    const lang  = this.translateConfigService.selected;
    this.user = this.authService.userData;
    const userId  = this.user.user_info.ID;
    
    return this.http.post(this.env.API_URL + 'lesson',
    { id, moduleId, userId, lang }
    ).pipe(
      tap(res => {
        const storageSlug   =   'lesson' + '_' + lang + '_' + id + '_' + moduleId + '_' + userId;
        if ( netWorkStatus === 'success') {
          this.storage.set(storageSlug, res);
          this.data = res;
        } else {
          this.data = this.storage.get(storageSlug);
          this.storage.get(storageSlug).then((val) => {
          }, err => {
            this.translateConfigService.presentAlert();
          });
        }
      }),
    );

  }

  getQuiz( moduleId, id ) {

    let res;
    const netWorkStatus = this.translateConfigService.networkCheck();
    const lang  = this.translateConfigService.selected;
    this.user = this.authService.userData;
    const userId  = this.user.user_info.ID;
    const storageSlug   =   'quiz' + '_' + lang + '_' + id + '_' + moduleId + '_' + userId;
    if ( netWorkStatus === 'success') {
      res = this.translateConfigService.httpPost(this.env.API_URL + 'quiz', { id, moduleId, userId, lang }, storageSlug);
    } else {
      res = this.translateConfigService.getDatafromLocalStorage ( storageSlug );
    }
    return res;
  }

  getQuizNew(  moduleId, id  ) {

    let res;
    const netWorkStatus = this.translateConfigService.networkCheck();
    const lang  = this.translateConfigService.selected;
    this.user = this.authService.userData;
    const userId  = this.user.user_info.ID;
    

    return this.http.post(this.env.API_URL + 'quiz',
    { id, moduleId, userId, lang }
    ).pipe(
      tap(res => {
        const storageSlug   =   'quiz' + '_' + lang + '_' + id + '_' + moduleId + '_' + userId;
        if ( netWorkStatus === 'success') {
          this.storage.set(storageSlug, res);
          this.data = res;
        } else {
          this.data = this.storage.get(storageSlug);
          this.storage.get(storageSlug).then((val) => {
          }, err => {
            this.translateConfigService.presentAlert();
          });
        }
      }),
    );

  }

  submitQuiz(form, moduleId, lessonId) {

    const netWorkStatus = this.translateConfigService.networkCheck();
    this.user = this.authService.userData;
    const userId  = this.user.user_info.ID;
    if ( netWorkStatus === 'error') {
      this.translateConfigService.presentAlert();
    }
    return this.translateConfigService.httpPost(this.env.API_URL + 'saveQuiz', { form , userId, moduleId, lessonId}, '');
  }

  getCertificate( moduleid ) {

    const netWorkStatus =  this.translateConfigService.networkCheck();
    this.user = this.authService.userData;
    const userid  = this.user.user_info.ID;
    if ( netWorkStatus === 'error') {
      this.translateConfigService.presentAlert();
    }
    return this.translateConfigService.httpPost(this.env.API_URL + 'getCertificatepdf', {userid, moduleid}, '');
  }
}
