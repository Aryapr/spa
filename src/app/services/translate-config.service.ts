import { Platform } from '@ionic/angular';
import { Injectable, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';
import {DOCUMENT} from '@angular/common';
import { BehaviorSubject} from 'rxjs';
import { AlertController } from '@ionic/angular';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HTTP } from '@ionic-native/http/ngx';
const LNG_KEY = 'SELECTED_LANGUAGE';

@Injectable({
  providedIn: 'root'
})
export class TranslateConfigService {
  headers;
  selected = 'en';
  public selectedLanguage = new BehaviorSubject('en');
  public selectedFontSize = new BehaviorSubject(1);
  constructor(
    private translate: TranslateService,
    private storage: Storage,
    private platform: Platform,
    @Inject(DOCUMENT) private doc,
    private alertController: AlertController,
    private Http: HTTP
  ) {
    this.headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
      Accept: 'application/json'
    };
  }

  setInitialAppLanguage() {
    const language = this.translate.getBrowserLang();
    this.translate.setDefaultLang(language);

    this.storage.get(LNG_KEY).then( val => {
      if (val) {
        this.setLanguage(val);
        this.selected = val;
      }
    });
  }

  setLanguage(setLang) {
    this.translate.use(setLang);
    this.selected = setLang;
    this.storage.set(LNG_KEY, setLang);
    this.selectedLanguage.next(setLang);

    // if (setLang === 'ar') {
    //   this.doc.dir = 'rtl';
    // } else {
    //   this.doc.dir = 'ltr';
    // }
  }

  setFontSize(value) {
    this.selectedFontSize.next(value);
  }
  getMessages(key) {
    return this.translate.instant(key);
  }

  networkCheck() {
    const networkCheck	= navigator.onLine;
    if (networkCheck === true) {
      //GO 
      return 'success';
    } else {
      return 'error';
      // this.presentAlert();
    }
  }

  

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Connection Error!',
      message: 'No network connection.',
      buttons: ['OK']
    });

    await alert.present();
  }
  async presentAlerts(headerText, messageText) {
    const alert = await this.alertController.create({
      header: headerText,
      message: messageText,
      buttons: ['OK']
    });

    await alert.present();
  }


  async httpPost( url? , data? , slug? ): Promise<any> {

    const self = this;
    const storageSlug = slug;
    return await new Promise( (resolve) => {
      self.Http.post( url , data, self.headers )
        .then(res => {
          console.log(res);
          const response = JSON.parse(res.data);
          console.log('after post', storageSlug);
          if (storageSlug) {
            this.storage.set(storageSlug, response).then(
              () => {
                console.log('data  Stored');
              },
              error => console.error('Error storing user info', error)
            );
          }
          resolve(response);
        })
        .catch(error => {
          resolve(error);
        });
    });

  }

  async getDatafromLocalStorage( storageSlug ) {

    const self = this;
    return new Promise( (resolve) => {
      self.storage.get(storageSlug)
        .then(res => {
          console.log(res);
          // const response = res;
          resolve(res);
        })
        .catch(error => {
          console.log(error);
          resolve(error);
        });
    });
  }

}
