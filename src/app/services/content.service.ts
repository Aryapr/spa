import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { EnvService } from './env.service';
import { TranslateConfigService } from './translate-config.service';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { HTTP } from '@ionic-native/http/ngx';

import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class ContentService {
  headers;
  data;
  constructor(
    private http: HttpClient,
    private env: EnvService,
    private translateConfigService: TranslateConfigService,
    private Http: HTTP,
    private storage: Storage
  ) {
    this.headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
      Accept: 'application/json'
    };
    // this.Http.setRequestTimeout(10.0);
   }

  getContents(page) {

    const lang = this.translateConfigService.selected;
    const netWorkStatus =  this.translateConfigService.networkCheck();
    let res;
    const storageSlug   =   (page === 'about-us' ? 'about' : page) + '_' + lang;

    if ( netWorkStatus === 'success') {
      res = this.translateConfigService.httpPost(this.env.API_URL + 'contents' , {page, lang}, storageSlug); 
    } else {
      res = this.translateConfigService.getDatafromLocalStorage ( storageSlug );
    }
    return res;
  }

  getContentsNew(page) {

    const netWorkStatus = this.translateConfigService.networkCheck();
    const lang = this.translateConfigService.selected;
    return this.http.post(this.env.API_URL + 'contents',
    {page, lang}
    ).pipe(
      tap(res => {
        const storageSlug   =   (page === 'about-us' ? 'about' : page) + '_' + lang;
        if ( netWorkStatus === 'success') {
          this.storage.set(storageSlug, (res));
          this.data = res;
        } else {
          res = this.translateConfigService.getDatafromLocalStorage ( storageSlug );
        }
      }),
    );

  }

  getResContents(page, province) {
    const lang = this.translateConfigService.selected;
    const netWorkStatus = this.translateConfigService.networkCheck();
    let res;
    const storageSlug = (page === 'about-us' ? 'about' : page) + '_' + lang;

    if (netWorkStatus === 'success') {
      res = this.translateConfigService.httpPost(
        this.env.API_URL + 'contents',
        { page, lang, province },
        storageSlug
      );
    } else {
      res = this.translateConfigService.getDatafromLocalStorage(storageSlug);
    }
    return res;
  }

  getResContentsNew(page, province) {
    // console.log(7878, province);
    const netWorkStatus = this.translateConfigService.networkCheck();
    const lang = this.translateConfigService.selected;
    return this.http
      .post(this.env.API_URL + 'contents', { page, lang, province })
      .pipe(
        tap(res => {
          const storageSlug =
            (page === 'about-us' ? 'about' : page) +
            '_' +
            lang +
            '_' +
            province;
          if (netWorkStatus === 'success') {
            this.storage.set(storageSlug, res);
            this.data = res;
          } else {
            res = this.translateConfigService.getDatafromLocalStorage(
              storageSlug
            );
          }
        })
      );
  }



  getMapContents(id, state, selected) {

    let res;
    const netWorkStatus = this.translateConfigService.networkCheck();
    const lang = this.translateConfigService.selected;
    const storageSlug   =   'map' + '_' + '_' + lang + id + '_' + state + '_' + selected;
    const debug = 1;
    if ( netWorkStatus === 'success') {
        res = this.translateConfigService.httpPost(this.env.API_URL + 'mapContents' ,
              {id, state, selected, lang, debug}, storageSlug
              );

    } else {
      res = this.translateConfigService.getDatafromLocalStorage ( storageSlug );
    }
    return res;
  }
  getMapContentsNew(id, state, selected) {

    const netWorkStatus = this.translateConfigService.networkCheck();
    const lang = this.translateConfigService.selected;
    return this.http.post(this.env.API_URL + 'mapContents',
    {id, state, selected, lang},
    ).pipe(
      tap(res => {
        const storageSlug   =   map + '_' + id + '_' + state + '_' + selected;
        if ( netWorkStatus === 'success') {
          this.storage.set(storageSlug, (res));
          this.data = res;
        } else {
          res = this.translateConfigService.getDatafromLocalStorage ( storageSlug );
        }
      }),
    );

  }
}
