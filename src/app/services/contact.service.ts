import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { EnvService } from './env.service';
import { TranslateConfigService } from './translate-config.service';
@Injectable({
  providedIn: 'root'
})
export class ContactService {

  userData: any;
  constructor(
    private http: HttpClient,
    private storage: NativeStorage,
    private env: EnvService,
    private translateConfigService: TranslateConfigService,
  ) { }

  contact(firstName: string, lastName: string, yourEmail: string, phone: string, yourMessage: string) {

    const netWorkStatus = this.translateConfigService.networkCheck();
    if ( netWorkStatus === 'error') {
      this.translateConfigService.presentAlert();
    }
    return this.translateConfigService.httpPost(this.env.API_URL + 'contact',
    {firstName, lastName, yourEmail, phone, yourMessage}, '');

    // return this.http.post(this.env.API_URL + 'contact',
    //   {firstName, lastName, yourEmail, phone, yourMessage}
    // ).pipe(
    //   tap(data => {

    //     this.userData = data;
    //     // return this.userData;
    //   }),
    // );
  }

}
