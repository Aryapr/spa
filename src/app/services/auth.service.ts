import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Storage } from '@ionic/storage';
import { EnvService } from './env.service';
import { ToastController, Platform } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { TranslateConfigService } from './translate-config.service';
import { HTTP } from '@ionic-native/http/ngx';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLoggedIn = new BehaviorSubject(false);
  userData: any;
  user: any;
  headers;
  data;
  constructor(
    private http: HttpClient,
    private storage: NativeStorage,
    private istorage: Storage,
    private platform: Platform,
    private env: EnvService,
    private router: Router,
    private translateConfigService: TranslateConfigService,
    private Http: HTTP,

  ) {
    this.headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
      Accept: 'application/json'
    };

    this.platform.ready().then(() => {
      this.getToken();
    });
  }

  login(username: string, password: string) {

    const self  = this;
    const url   = this.env.API_URL + 'login';
    const data  = {username, password};
    const netWorkStatus = this.translateConfigService.networkCheck();
    console.log(netWorkStatus);
    if ( netWorkStatus === 'error') {
      this.translateConfigService.presentAlert();
    }
    return new Promise( (resolve) => {
      self.Http.post( url , data, self.headers )
        .then(res => {
          console.log(res);
          const response = JSON.parse(res.data);
          this.userData = response;
          if (this.userData.loggedin === 1 ) {
            this.isLoggedIn.next(true);
            this.istorage.set('SPA_USER_INFO', this.userData)
            .then(
              () => {
                console.log('user info Stored');
              },
              error => console.error('Error storing user info', error)
            );
          }
          resolve(response);
        })
        .catch(error => {
          resolve(error);
        });
    });
  }


  /** For browser purpose only in ionic serve */
  loginNew(username: string, password: string) {

    const self  = this;
    const url   = this.env.API_URL + 'login';
    const data  = {username, password};
    const netWorkStatus = this.translateConfigService.networkCheck();
    console.log(netWorkStatus);
    if ( netWorkStatus === 'error') {
      this.translateConfigService.presentAlert();
    }
    return new Promise( (resolve) => {
      self.http.post( url , data, self.headers ).toPromise().then((res) => {
        console.log(res);
        const response = res;
        this.userData = response;
        if (this.userData.loggedin === 1 ) {
          this.isLoggedIn.next(true);
          this.istorage.set('SPA_USER_INFO', this.userData)
          .then(
            () => {
              console.log('user info Stored');
            },
            error => console.error('Error storing user info', error)
          );
        }
        resolve(response);
      }, (error) => {
        console.log('Promise rejected with ' + JSON.stringify(error));
      });
    });
  }

  register(firstName: string, lastName: string, uciNumber: string, dob: string, email: string, phone: string,
           messageType: string, recCheck: string, oopCheck: string, sopaRegister: string) {
      const lang = this.translateConfigService.selected;

      const netWorkStatus = this.translateConfigService.networkCheck();
      if ( netWorkStatus === 'error') {
        this.translateConfigService.presentAlert();
      }
      return this.translateConfigService.httpPost(this.env.API_URL + 'register',
      {firstName, lastName, uciNumber, dob, email, phone, messageType, recCheck, oopCheck, sopaRegister, lang}, '');
  }

  forgot(email: string) {

    const netWorkStatus = this.translateConfigService.networkCheck();
    if ( netWorkStatus === 'error') {
      this.translateConfigService.presentAlert();
    }
    return this.translateConfigService.httpPost(this.env.API_URL + 'forgot',
      {email}, ''
    );
  }

  getUser() {

    return this.istorage.get('SPA_USER_INFO').then(
      data => {
        this.userData = data;
      },
      error => {
        this.userData = null;
        this.isLoggedIn.next(false);
      }
    );
  }

  getToken() {
    return this.istorage.get('SPA_USER_INFO').then(
      data => {
        this.userData = data;
        if (this.userData != null) {
          this.isLoggedIn.next(true);
        } else {
          this.isLoggedIn.next(false);
        }
      },
      error => {
        this.userData = null;
        this.isLoggedIn.next(false);
      }
    );
  }
  isAuthenticated() {
    return this.isLoggedIn.value;
  }

  logout() {
    this.istorage.remove('SPA_USER_INFO').then(() => {
      this.router.navigate(['/start/tabs/login']);
      this.isLoggedIn.next(false);
    });
  }

  getAccountInfo( userid ){
    let res;
    const netWorkStatus = this.translateConfigService.networkCheck();
    const lang = this.translateConfigService.selected;
    const storageSlug   =   'getAccountInfo' + '_' + lang + '_' + userid;
    if ( netWorkStatus === 'success') {
      res = this.translateConfigService.httpPost(this.env.API_URL + 'getAccountInfo',
      { userid, lang }, storageSlug
      );
    } else {
      res = this.translateConfigService.getDatafromLocalStorage ( storageSlug );
    }
    return res;
  }

  getAccountInfoNew( userid ){
    let res;
    const netWorkStatus = this.translateConfigService.networkCheck();
    const lang = this.translateConfigService.selected;
    const storageSlug   =   'getAccountInfo' + '_' + lang + '_' + userid;
      return this.http.post(this.env.API_URL + 'getAccountInfo',
      { userid, lang }).pipe(
        tap(res => {
          //const storageSlug   =   map + '_' + id + '_' + state + '_' + selected;
          if ( netWorkStatus === 'success') {
           // this.storage.set(storageSlug, (res));
            this.data = res;
          } else {
            res = this.translateConfigService.getDatafromLocalStorage ( storageSlug );
          }
        }),
      );
  }

  myaccountSave(userId: string, firstName: string, lastName: string, uciNumber: string, dob: string, passCheck: string,
                password: string, newPassword: string, confirmPassword: string) {
    const netWorkStatus =  this.translateConfigService.networkCheck();
    if ( netWorkStatus === 'error') {
      this.translateConfigService.presentAlert();
    }
    return this.translateConfigService.httpPost(this.env.API_URL + 'myaccountSave',
    {userId, firstName, lastName, uciNumber, dob, passCheck, password, newPassword, confirmPassword}, ''
    );
  }

  getCertificatepdf( moduleid  , userid ) {
    const netWorkStatus =  this.translateConfigService.networkCheck();
    return this.translateConfigService.httpPost(this.env.API_URL + 'getCertificatepdf',
    { moduleid , userid }, ''
    );
  }

}
