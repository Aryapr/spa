import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';

import { ContactService } from 'src/app/services/contact.service';
import { AlertService } from 'src/app/services/alert.service';
import { ContentService } from '../services/content.service';
import { TranslateConfigService } from '../services/translate-config.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {

  contactForm: FormGroup;
  userData: any;
  messages: any;
  subscribeObj;
  subscribeObj1;
  selectedFontSize  = 1;
  isFetched = false;
  contactData = {
    // tslint:disable-next-line:max-line-length
    contact_address_detail: '<a href="https://www.google.com/maps/place/206+Keefer+St,+Vancouver,+BC+V6A+1X6,+Canada/@49.2793371,-123.1014002,17z/data=!3m1!4b1!4m5!3m4!1s0x548671701bad8ccb:0x76d307c057fb63e3!8m2!3d49.2793371!4d-123.0992115"  item-center>#206 – 181 Keefer Place, <br/> Vancouver BC, V6B 6C1</a>',
    contact_phone: '<a href="tel:12368801038">+1 236 880 1038</a>',
    contact_email: '<a href="mailto:estartcanada@success.bc.ca">estartcanada@success.bc.ca</a>'
  };
  constructor(
    private contactService: ContactService,
    private navCtrl: NavController,
    private alertService: AlertService,
    private formBuilder: FormBuilder,
    public loadingController: LoadingController,
    private translateConfigService: TranslateConfigService,
    private content: ContentService,
    private storage: Storage
  ) {

    this.contactForm = this.formBuilder.group({
      first_name    : ['', Validators.compose([Validators.required])],
      last_name : ['', Validators.compose([Validators.required])],
      your_email    : ['', Validators.compose([Validators.required, Validators.email])],
      phone    : ['', Validators.compose([Validators.required])],
      your_message    : ['', Validators.compose([Validators.required])],
    });
  }
  ionViewWillEnter() {
    // Load the data
    this.subscribeObj  = this.translateConfigService.selectedLanguage.subscribe(lang => {
      this.getContents();
    });
    this.subscribeObj1 = this.translateConfigService.selectedFontSize.subscribe(size => {
      this.selectedFontSize = size;
      document.body.style.setProperty('--pixels', this.selectedFontSize.toString());
    });

  }
  ionViewWillLeave() {
    this.subscribeObj.unsubscribe();
    this.subscribeObj1.unsubscribe();
  }
  ngOnInit() {
  }

  async getContents() {
    const loading = await this.loadingController.create({
      message: this.translateConfigService.getMessages('loading')
    });

    /** Get data from local storage */
    const page = 'contact-us';
    const lang = this.translateConfigService.selected;
    const storageSlug   =   page + '_' + lang;
   // const contactData = await this.storage.get(storageSlug);
    // if (this.contactData) {
    //   this.contactData = this.contactData[0];
    // } else {
    //   this.contactData = {
    //     // tslint:disable-next-line:max-line-length
    //     contact_address_detail: '<a href="https://www.google.com/maps/place/206+Keefer+St,+Vancouver,+BC+V6A+1X6,+Canada/@49.2793371,-123.1014002,17z/data=!3m1!4b1!4m5!3m4!1s0x548671701bad8ccb:0x76d307c057fb63e3!8m2!3d49.2793371!4d-123.0992115"  item-center>#206 – 181 Keefer Place, <br/> Vancouver BC, V6B 6C1</a>',
    //     contact_phone: '<a href="tel:1 778 832 0183">+1 778 832 0183</a>',
    //     contact_email: '<a href="mailto:estartcanada@success.bc.ca">estartcanada@success.bc.ca</a>'
    //   };
    //   this.isFetched = false;
    // }

    // if (!this.isFetched) {
    //   await loading.present();
    // }
    await loading.present();
    await this.content.getContents(page)
      .then(
        data => {
          if ( data ) {
          // Set the data to display in the template
            this.contactData = data[0];
            this.isFetched = true;
          }
          loading.dismiss();
        },
        err => {
          console.log(err);
          loading.dismiss();
        }
    );
  }

  contact(form) {
    this.messages = this.translateConfigService.getMessages('CONTACT.messages');
    this.contactService.contact(form.value.first_name, form.value.last_name, form.value.your_email, 
      form.value.phone, form.value.your_message).then(
      data => {
        this.userData = data;
        this.alertService.presentToast(this.messages[this.userData.status]);
        if (this.userData.status === 1) {
          this.contactForm.reset();
          this.navCtrl.navigateRoot('start/tabs/contact');
        }
      },
      error => {
        console.log(error);
      },
      // () => {
      // }
    );
  }

}
